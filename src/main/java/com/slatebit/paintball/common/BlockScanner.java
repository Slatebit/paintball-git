package com.slatebit.paintball.common;

import java.util.Iterator;
import java.util.List;
import net.minecraft.block.Block;
import net.minecraft.block.BlockPressurePlate;
import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyEnum;
import net.minecraft.block.state.BlockState;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class BlockScanner extends BlockPressurePlate
{
	public static final PropertyEnum COLOR = PropertyEnum.create("color", EnumColor.class);
	private final BlockPressurePlate.Sensitivity sensitivity;

	public BlockScanner(Material material, BlockPressurePlate.Sensitivity sensitivity)
	{
		super(material, sensitivity);
		this.setHardness(0.8F);
		this.sensitivity = sensitivity;
		this.setStepSound(Block.soundTypeCloth);
		this.setCreativeTab(Paintball.TAB);
		GameRegistry.registerBlock(this, ItemMetadataBlock.class, "scanner");
	}

	@Override
	public int computeRedstoneStrength(World world, BlockPos pos)
	{
		AxisAlignedBB axisalignedbb = this.getSensitiveAABB(pos);
		List list;
		switch (this.sensitivity)
		{
		case EVERYTHING:
			list = world.getEntitiesWithinAABBExcludingEntity((Entity) null, axisalignedbb);
		break;
		case MOBS:
			list = world.getEntitiesWithinAABB(EntityLivingBase.class, axisalignedbb);
		break;
		default:
			return 0;
		}
		if (!list.isEmpty())
		{
			Iterator iterator = list.iterator();
			while (iterator.hasNext())
			{
				Entity entity = (Entity) iterator.next();
				if (entity instanceof EntityPlayer && !entity.doesEntityNotTriggerPressurePlate())
				{
					int color = ((EnumColor) world.getBlockState(pos).getValue(BlockScanner.COLOR)).getMetadata();
					EntityPlayer entityplayer = (EntityPlayer) entity;
					if (Paintball.isSameTeam(color, entityplayer.inventory.armorInventory))
					{
						return 15;
					}
				}
			}
		}
		return 0;
	}

	@Override
	public int damageDropped(IBlockState state)
	{
		return ((EnumColor) state.getValue(BlockScanner.COLOR)).getMetadata();
	}

	@Override
	public void getSubBlocks(Item item, CreativeTabs creativeTabs, List list)
	{
		for (int i = 0; i < 6; i ++)
		{
			list.add(new ItemStack(item, 1, i));
		}
	}

	@Override
	public MapColor getMapColor(IBlockState state)
	{
		return ((EnumColor) state.getValue(BlockScanner.COLOR)).getMapColor();
	}

	@Override
	public IBlockState getStateFromMeta(int meta)
	{
		EnumColor enumColor;
		switch (meta & 7)
		{
		case 0:
			enumColor = EnumColor.RED;
		break;
		case 1:
			enumColor = EnumColor.ORANGE;
		break;
		case 2:
			enumColor = EnumColor.YELLOW;
		break;
		case 3:
			enumColor = EnumColor.GREEN;
		break;
		case 4:
			enumColor = EnumColor.BLUE;
		break;
		case 5:
		default:
			enumColor = EnumColor.PURPLE;
		}
		return this.getDefaultState().withProperty(BlockScanner.COLOR, enumColor).withProperty(BlockPressurePlate.POWERED, Boolean.valueOf((meta & 8) > 0));
	}

	@Override
	public int getMetaFromState(IBlockState state)
	{
		int i;
		switch ((EnumColor) state.getValue(BlockScanner.COLOR))
		{
		case RED:
			i = 1;
		break;
		case ORANGE:
			i = 2;
		break;
		case YELLOW:
			i = 3;
		break;
		case GREEN:
			i = 4;
		break;
		case BLUE:
		default:
			i = 5;
		break;
		case PURPLE:
			i = 0;
		}
		if (((Boolean) state.getValue(BlockPressurePlate.POWERED)).booleanValue())
		{
			i |= 8;
		}
		return i;
	}

	@Override
	protected BlockState createBlockState()
	{
		return new BlockState(this, new IProperty[] {BlockPressurePlate.POWERED, BlockScanner.COLOR});
	}
}