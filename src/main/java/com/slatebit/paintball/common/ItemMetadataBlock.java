package com.slatebit.paintball.common;

import java.util.List;
import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ItemMetadataBlock extends ItemBlock
{
	public ItemMetadataBlock(Block block)
	{
		super(block);
		this.setHasSubtypes(true);
		this.setMaxDamage(0);
	}

	@Override
	public int getMetadata(int metadata)
	{
		return metadata;
	}

	@Override
	public void getSubItems(Item item, CreativeTabs creativeTabs, List list)
	{
		for (int i = 0; i < 6; i ++)
		{
			list.add(new ItemStack(item, 1, i));
		}
	}

	@Override
	@SideOnly(Side.CLIENT)
	public String getItemStackDisplayName(ItemStack itemStack)
	{
		String name = null;
		int color = itemStack.getItemDamage();
		Item item = itemStack.getItem();
		if (item == Item.getItemFromBlock(Paintball.scanner))
		{
			name = "Scanner";
		}
		else if (item == Item.getItemFromBlock(Paintball.instaBase))
		{
			name = "Insta-Base";
		}
		else if (item == Item.getItemFromBlock(Paintball.claymore))
		{
			name = "Claymore";
		}
		else if (item == Item.getItemFromBlock(Paintball.c4))
		{
			name = "C4";
		}
		else if (item == Item.getItemFromBlock(Paintball.pod))
		{
			name = "Pod";
		}
		else if (item == Item.getItemFromBlock(Paintball.weaponRack))
		{
			name = "Weapon Rack";
		}
		else if (item == Item.getItemFromBlock(Paintball.gearRack))
		{
			name = "Gear Rack";
		}
		else if (item == Item.getItemFromBlock(Paintball.flag))
		{
			name = "Flag";
		}
		else if (item == Item.getItemFromBlock(Paintball.medKit))
		{
			name = "Med Kit";
		}
		if (name == "Claymore" || name == "C4")
		{
			this.setMaxStackSize(2);
		}
		return name + " (" + Paintball.COLOR_MAP.get(color).charAt(0) + ")";
	}
}