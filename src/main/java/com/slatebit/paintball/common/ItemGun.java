package com.slatebit.paintball.common;

import java.util.List;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.lwjgl.input.Mouse;

public class ItemGun extends Item
{
	public int damage;
	public int delay;
	public int distance;
	public String type;
	private boolean hasShot;

	public ItemGun(int damage, int delay, int distance, String type)
	{
		super();
		this.setHasSubtypes(true);
		this.maxStackSize = 1;
		this.setCreativeTab(Paintball.TAB);
		this.damage = damage;
		this.delay = delay;
		this.distance = distance;
		this.type = type;
		GameRegistry.registerItem(this, type.toLowerCase());
	}

	@Override
	public void onUpdate(ItemStack itemStack, World world, Entity entity, int inventoryIndex, boolean flag)
	{
		if (world.isRemote && !Mouse.isButtonDown(1))
		{
			this.hasShot = false;
		}
	}

	@Override
	public ItemStack onItemRightClick(ItemStack itemStack, World world, EntityPlayer entityPlayer)
	{
		if (world.isRemote && Paintball.shootTime <= 0 && !this.hasShot)
		{
			Paintball.NETWORK_HELPER.sendPacketToServer(new PacketHandler(entityPlayer.getDisplayNameString(), 1));
			Paintball.shootTime = this.delay;
			this.hasShot = true;
		}
		return itemStack;
	}

	@Override
	public float getDigSpeed(ItemStack itemstack, IBlockState state)
	{
		return 0;
	}

	@Override
	public void getSubItems(Item item, CreativeTabs creativeTabs, List list)
	{
		for (int i = 0; i < 6; i ++)
		{
			list.add(new ItemStack(item, 1, i));
		}
	}

	@Override
	@SideOnly(Side.CLIENT)
	public String getItemStackDisplayName(ItemStack itemStack)
	{
		return this.type + " (" + Paintball.COLOR_MAP.get(itemStack.getItemDamage()).charAt(0) + ")";
	}
}