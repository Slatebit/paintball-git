package com.slatebit.paintball.common;

import java.util.List;
import net.minecraft.block.Block;
import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyEnum;
import net.minecraft.block.state.BlockState;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntityChest;
import net.minecraft.util.BlockPos;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class BlockInstaBase extends Block
{
	public static final PropertyEnum COLOR = PropertyEnum.create("color", EnumColor.class);

	public BlockInstaBase()
	{
		super(Material.wood);
		this.setHardness(2.5F);
		this.setStepSound(Block.soundTypeWood);
		this.setCreativeTab(Paintball.TAB);
		GameRegistry.registerBlock(this, ItemMetadataBlock.class, "insta-base");
	}

	@Override
	public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer entityPlayer, EnumFacing face, float xPosition, float yPosition, float zPosition)
	{
		InventoryPlayer inventory = entityPlayer.inventory;
		if (!world.isRemote && (inventory.getCurrentItem() == null || inventory.getCurrentItem() != null && !(inventory.getCurrentItem().getItem() instanceof ItemPaintbrush)))
		{
			int x = pos.getX();
			int y = pos.getY();
			int z = pos.getZ();
			int metadata = this.getMetaFromState(state);
			world.setBlockToAir(pos);
			for (int i = x - 3; i <= x + 3; i ++)
			{
				for (int j = y; j <= y + 6; j ++)
				{
					for (int k = z - 3; k <= z + 3; k ++)
					{
						Block block = world.getBlockState(new BlockPos(i, j, k)).getBlock();
						if (block.getMaterial().isSolid() || block == Blocks.snow_layer)
						{
							world.setBlockState(pos, this.getStateFromMeta(metadata), 3);
							entityPlayer.addChatMessage(new ChatComponentText("There isn't enough space for the Insta-Base to be created."));
							return false;
						}
					}
				}
			}
			this.spawnBlock(world, pos, metadata, entityPlayer);
			return true;
		}
		return false;
	}

	public void spawnBlock(World world, BlockPos pos, int metadata, EntityPlayer entityPlayer)
	{
		int x = pos.getX();
		int y = pos.getY();
		int z = pos.getZ();
		int color = Paintball.WOOL_MAP.get(Paintball.COLOR_MAP.get(metadata));
		IBlockState woolState = Blocks.wool.getStateFromMeta(color);
		IBlockState scannerState = Paintball.scanner.getStateFromMeta(metadata);
		IBlockState stairsState = Blocks.oak_stairs.getStateFromMeta(3);
		IBlockState glassState = Blocks.stained_glass.getStateFromMeta(color);
		world.setBlockState(new BlockPos(x, y - 1, z), woolState, 3);
		world.setBlockState(new BlockPos(x, y - 1, z + 1), woolState, 3);
		world.setBlockState(new BlockPos(x, y - 1, z + 2), woolState, 3);
		world.setBlockState(new BlockPos(x, y - 1, z + 3), woolState, 3);
		world.setBlockState(new BlockPos(x, y - 1, z - 1), woolState, 3);
		world.setBlockState(new BlockPos(x, y - 1, z - 2), woolState, 3);
		world.setBlockState(new BlockPos(x, y - 1, z - 3), woolState, 3);
		world.setBlockState(new BlockPos(x + 1, y - 1, z), woolState, 3);
		world.setBlockState(new BlockPos(x + 1, y - 1, z + 1), woolState, 3);
		world.setBlockState(new BlockPos(x + 1, y - 1, z + 2), woolState, 3);
		world.setBlockState(new BlockPos(x + 1, y - 1, z + 3), woolState, 3);
		world.setBlockState(new BlockPos(x + 1, y - 1, z + 4), woolState, 3);
		world.setBlockState(new BlockPos(x + 1, y - 1, z - 1), woolState, 3);
		world.setBlockState(new BlockPos(x + 1, y - 1, z - 2), woolState, 3);
		world.setBlockState(new BlockPos(x + 1, y - 1, z - 3), woolState, 3);
		world.setBlockState(new BlockPos(x + 2, y - 1, z), woolState, 3);
		world.setBlockState(new BlockPos(x + 2, y - 1, z + 1), woolState, 3);
		world.setBlockState(new BlockPos(x + 2, y - 1, z + 2), woolState, 3);
		world.setBlockState(new BlockPos(x + 2, y - 1, z + 3), woolState, 3);
		world.setBlockState(new BlockPos(x + 2, y - 1, z - 1), woolState, 3);
		world.setBlockState(new BlockPos(x + 2, y - 1, z - 2), woolState, 3);
		world.setBlockState(new BlockPos(x + 2, y - 1, z - 3), woolState, 3);
		world.setBlockState(new BlockPos(x + 3, y - 1, z), woolState, 3);
		world.setBlockState(new BlockPos(x + 3, y - 1, z + 1), woolState, 3);
		world.setBlockState(new BlockPos(x + 3, y - 1, z + 2), woolState, 3);
		world.setBlockState(new BlockPos(x + 3, y - 1, z - 1), woolState, 3);
		world.setBlockState(new BlockPos(x + 3, y - 1, z - 2), woolState, 3);
		world.setBlockState(new BlockPos(x + 4, y - 1, z - 1), woolState, 3);
		world.setBlockState(new BlockPos(x - 1, y - 1, z), woolState, 3);
		world.setBlockState(new BlockPos(x - 1, y - 1, z + 1), woolState, 3);
		world.setBlockState(new BlockPos(x - 1, y - 1, z + 2), woolState, 3);
		world.setBlockState(new BlockPos(x - 1, y - 1, z + 3), woolState, 3);
		world.setBlockState(new BlockPos(x - 1, y - 1, z - 1), woolState, 3);
		world.setBlockState(new BlockPos(x - 1, y - 1, z - 2), woolState, 3);
		world.setBlockState(new BlockPos(x - 1, y - 1, z - 3), woolState, 3);
		world.setBlockState(new BlockPos(x - 1, y - 1, z - 4), woolState, 3);
		world.setBlockState(new BlockPos(x - 2, y - 1, z), woolState, 3);
		world.setBlockState(new BlockPos(x - 2, y - 1, z + 1), woolState, 3);
		world.setBlockState(new BlockPos(x - 2, y - 1, z + 2), woolState, 3);
		world.setBlockState(new BlockPos(x - 2, y - 1, z + 3), woolState, 3);
		world.setBlockState(new BlockPos(x - 2, y - 1, z - 1), woolState, 3);
		world.setBlockState(new BlockPos(x - 2, y - 1, z - 2), woolState, 3);
		world.setBlockState(new BlockPos(x - 2, y - 1, z - 3), woolState, 3);
		world.setBlockState(new BlockPos(x - 3, y - 1, z), woolState, 3);
		world.setBlockState(new BlockPos(x - 3, y - 1, z + 1), woolState, 3);
		world.setBlockState(new BlockPos(x - 3, y - 1, z + 2), woolState, 3);
		world.setBlockState(new BlockPos(x - 3, y - 1, z - 1), woolState, 3);
		world.setBlockState(new BlockPos(x - 3, y - 1, z - 2), woolState, 3);
		world.setBlockState(new BlockPos(x - 4, y - 1, z + 1), woolState, 3);
		world.setBlockState(new BlockPos(x, y, z + 3), woolState, 3);
		world.setBlockState(new BlockPos(x, y, z - 3), woolState, 3);
		world.setBlockState(new BlockPos(x + 1, y, z + 2), scannerState, 3);
		world.setBlockState(new BlockPos(x + 1, y, z + 4), scannerState, 3);
		world.setBlockState(new BlockPos(x + 1, y, z - 3), woolState, 3);
		world.setBlockState(new BlockPos(x + 2, y, z + 1), stairsState, 3);
		world.setBlockState(new BlockPos(x + 2, y, z - 1), scannerState, 3);
		world.setBlockState(new BlockPos(x + 2, y, z + 3), woolState, 3);
		world.setBlockState(new BlockPos(x + 2, y, z - 3), woolState, 3);
		world.setBlockState(new BlockPos(x + 3, y, z), woolState, 3);
		world.setBlockState(new BlockPos(x + 3, y, z + 1), woolState, 3);
		world.setBlockState(new BlockPos(x + 3, y, z + 2), woolState, 3);
		world.setBlockState(new BlockPos(x + 3, y, z - 2), woolState, 3);
		world.setBlockState(new BlockPos(x + 4, y, z - 1), scannerState, 3);
		world.setBlockState(new BlockPos(x - 1, y, z + 3), woolState, 3);
		world.setBlockState(new BlockPos(x - 1, y, z - 2), scannerState, 3);
		world.setBlockState(new BlockPos(x - 1, y, z - 4), scannerState, 3);
		world.setBlockState(new BlockPos(x - 2, y, z + 1), scannerState, 3);
		world.setBlockState(new BlockPos(x - 2, y, z + 3), woolState, 3);
		world.setBlockState(new BlockPos(x - 2, y, z - 3), woolState, 3);
		world.setBlockState(new BlockPos(x - 3, y, z), woolState, 3);
		world.setBlockState(new BlockPos(x - 3, y, z + 2), woolState, 3);
		world.setBlockState(new BlockPos(x - 3, y, z - 1), woolState, 3);
		world.setBlockState(new BlockPos(x - 3, y, z - 2), woolState, 3);
		world.setBlockState(new BlockPos(x - 4, y, z + 1), scannerState, 3);
		world.setBlockState(new BlockPos(x, y + 1, z + 3), woolState, 3);
		world.setBlockState(new BlockPos(x, y + 1, z - 3), woolState, 3);
		world.setBlockState(new BlockPos(x + 1, y + 1, z - 3), woolState, 3);
		world.setBlockState(new BlockPos(x + 2, y + 1, z), stairsState, 3);
		world.setBlockState(new BlockPos(x + 2, y + 1, z + 3), woolState, 3);
		world.setBlockState(new BlockPos(x + 2, y + 1, z - 3), woolState, 3);
		world.setBlockState(new BlockPos(x + 3, y + 1, z), woolState, 3);
		world.setBlockState(new BlockPos(x + 3, y + 1, z + 1), woolState, 3);
		world.setBlockState(new BlockPos(x + 3, y + 1, z + 2), woolState, 3);
		world.setBlockState(new BlockPos(x + 3, y + 1, z - 2), woolState, 3);
		world.setBlockState(new BlockPos(x - 1, y + 1, z + 3), woolState, 3);
		world.setBlockState(new BlockPos(x - 2, y + 1, z + 3), woolState, 3);
		world.setBlockState(new BlockPos(x - 2, y + 1, z - 3), woolState, 3);
		world.setBlockState(new BlockPos(x - 3, y + 1, z), woolState, 3);
		world.setBlockState(new BlockPos(x - 3, y + 1, z + 2), woolState, 3);
		world.setBlockState(new BlockPos(x - 3, y + 1, z - 1), woolState, 3);
		world.setBlockState(new BlockPos(x - 3, y + 1, z - 2), woolState, 3);
		world.setBlockState(new BlockPos(x, y + 2, z + 3), woolState, 3);
		world.setBlockState(new BlockPos(x, y + 2, z - 3), woolState, 3);
		world.setBlockState(new BlockPos(x + 1, y + 2, z + 3), woolState, 3);
		world.setBlockState(new BlockPos(x + 1, y + 2, z - 3), woolState, 3);
		world.setBlockState(new BlockPos(x + 2, y + 2, z + 3), woolState, 3);
		world.setBlockState(new BlockPos(x + 2, y + 2, z - 1), stairsState, 3);
		world.setBlockState(new BlockPos(x + 2, y + 2, z - 3), woolState, 3);
		world.setBlockState(new BlockPos(x + 3, y + 2, z), woolState, 3);
		world.setBlockState(new BlockPos(x + 3, y + 2, z + 1), woolState, 3);
		world.setBlockState(new BlockPos(x + 3, y + 2, z + 2), woolState, 3);
		world.setBlockState(new BlockPos(x + 3, y + 2, z - 1), woolState, 3);
		world.setBlockState(new BlockPos(x + 3, y + 2, z - 2), woolState, 3);
		world.setBlockState(new BlockPos(x - 1, y + 2, z + 3), woolState, 3);
		world.setBlockState(new BlockPos(x - 1, y + 2, z - 3), woolState, 3);
		world.setBlockState(new BlockPos(x - 2, y + 2, z + 3), woolState, 3);
		world.setBlockState(new BlockPos(x - 2, y + 2, z - 3), woolState, 3);
		world.setBlockState(new BlockPos(x - 3, y + 2, z), woolState, 3);
		world.setBlockState(new BlockPos(x - 3, y + 2, z + 1), woolState, 3);
		world.setBlockState(new BlockPos(x - 3, y + 2, z + 2), woolState, 3);
		world.setBlockState(new BlockPos(x - 3, y + 2, z - 1), woolState, 3);
		world.setBlockState(new BlockPos(x - 3, y + 2, z - 2), woolState, 3);
		world.setBlockState(new BlockPos(x, y + 3, z), glassState, 3);
		world.setBlockState(new BlockPos(x, y + 3, z + 1), glassState, 3);
		world.setBlockState(new BlockPos(x, y + 3, z + 2), glassState, 3);
		world.setBlockState(new BlockPos(x, y + 3, z + 3), woolState, 3);
		world.setBlockState(new BlockPos(x, y + 3, z - 1), glassState, 3);
		world.setBlockState(new BlockPos(x, y + 3, z - 2), glassState, 3);
		world.setBlockState(new BlockPos(x, y + 3, z - 3), woolState, 3);
		world.setBlockState(new BlockPos(x + 1, y + 3, z), woolState, 3);
		world.setBlockState(new BlockPos(x + 1, y + 3, z + 1), woolState, 3);
		world.setBlockState(new BlockPos(x + 1, y + 3, z + 2), woolState, 3);
		world.setBlockState(new BlockPos(x + 1, y + 3, z + 3), woolState, 3);
		world.setBlockState(new BlockPos(x + 1, y + 3, z - 1), woolState, 3);
		world.setBlockState(new BlockPos(x + 1, y + 3, z - 2), woolState, 3);
		world.setBlockState(new BlockPos(x + 1, y + 3, z - 3), woolState, 3);
		world.setBlockState(new BlockPos(x + 2, y + 3, z + 3), woolState, 3);
		world.setBlockState(new BlockPos(x + 2, y + 3, z - 2), stairsState, 3);
		world.setBlockState(new BlockPos(x + 2, y + 3, z - 3), woolState, 3);
		world.setBlockState(new BlockPos(x + 3, y + 3, z), woolState, 3);
		world.setBlockState(new BlockPos(x + 3, y + 3, z + 1), woolState, 3);
		world.setBlockState(new BlockPos(x + 3, y + 3, z + 2), woolState, 3);
		world.setBlockState(new BlockPos(x + 3, y + 3, z - 1), woolState, 3);
		world.setBlockState(new BlockPos(x + 3, y + 3, z - 2), woolState, 3);
		world.setBlockState(new BlockPos(x - 1, y + 3, z), glassState, 3);
		world.setBlockState(new BlockPos(x - 1, y + 3, z + 1), glassState, 3);
		world.setBlockState(new BlockPos(x - 1, y + 3, z + 2), glassState, 3);
		world.setBlockState(new BlockPos(x - 1, y + 3, z + 3), woolState, 3);
		world.setBlockState(new BlockPos(x - 1, y + 3, z - 1), glassState, 3);
		world.setBlockState(new BlockPos(x - 1, y + 3, z - 2), glassState, 3);
		world.setBlockState(new BlockPos(x - 1, y + 3, z - 3), woolState, 3);
		world.setBlockState(new BlockPos(x - 2, y + 3, z), glassState, 3);
		world.setBlockState(new BlockPos(x - 2, y + 3, z + 1), glassState, 3);
		world.setBlockState(new BlockPos(x - 2, y + 3, z + 2), woolState, 3);
		world.setBlockState(new BlockPos(x - 2, y + 3, z + 3), woolState, 3);
		world.setBlockState(new BlockPos(x - 2, y + 3, z - 1), glassState, 3);
		world.setBlockState(new BlockPos(x - 2, y + 3, z - 2), woolState, 3);
		world.setBlockState(new BlockPos(x - 2, y + 3, z - 3), woolState, 3);
		world.setBlockState(new BlockPos(x - 3, y + 3, z), woolState, 3);
		world.setBlockState(new BlockPos(x - 3, y + 3, z + 1), woolState, 3);
		world.setBlockState(new BlockPos(x - 3, y + 3, z + 2), woolState, 3);
		world.setBlockState(new BlockPos(x - 3, y + 3, z - 1), woolState, 3);
		world.setBlockState(new BlockPos(x - 3, y + 3, z - 2), woolState, 3);
		world.setBlockState(new BlockPos(x, y + 4, z + 4), woolState, 3);
		world.setBlockState(new BlockPos(x, y + 4, z - 4), woolState, 3);
		world.setBlockState(new BlockPos(x + 1, y + 4, z + 4), woolState, 3);
		world.setBlockState(new BlockPos(x + 1, y + 4, z - 4), woolState, 3);
		world.setBlockState(new BlockPos(x + 2, y + 4, z + 4), woolState, 3);
		world.setBlockState(new BlockPos(x + 2, y + 4, z - 4), woolState, 3);
		world.setBlockState(new BlockPos(x + 3, y + 4, z + 3), woolState, 3);
		world.setBlockState(new BlockPos(x + 3, y + 4, z - 3), woolState, 3);
		world.setBlockState(new BlockPos(x + 4, y + 4, z), woolState, 3);
		world.setBlockState(new BlockPos(x + 4, y + 4, z + 1), woolState, 3);
		world.setBlockState(new BlockPos(x + 4, y + 4, z + 2), woolState, 3);
		world.setBlockState(new BlockPos(x + 4, y + 4, z - 1), woolState, 3);
		world.setBlockState(new BlockPos(x + 4, y + 4, z - 2), woolState, 3);
		world.setBlockState(new BlockPos(x - 1, y + 4, z + 4), woolState, 3);
		world.setBlockState(new BlockPos(x - 1, y + 4, z - 4), woolState, 3);
		world.setBlockState(new BlockPos(x - 2, y + 4, z + 4), woolState, 3);
		world.setBlockState(new BlockPos(x - 2, y + 4, z - 4), woolState, 3);
		world.setBlockState(new BlockPos(x - 3, y + 4, z + 3), woolState, 3);
		world.setBlockState(new BlockPos(x - 3, y + 4, z - 3), woolState, 3);
		world.setBlockState(new BlockPos(x - 4, y + 4, z), woolState, 3);
		world.setBlockState(new BlockPos(x - 4, y + 4, z + 1), woolState, 3);
		world.setBlockState(new BlockPos(x - 4, y + 4, z + 2), woolState, 3);
		world.setBlockState(new BlockPos(x - 4, y + 4, z - 1), woolState, 3);
		world.setBlockState(new BlockPos(x - 4, y + 4, z - 2), woolState, 3);
		world.setBlockState(new BlockPos(x + 1, y + 5, z + 4), woolState, 3);
		world.setBlockState(new BlockPos(x + 1, y + 5, z - 4), woolState, 3);
		world.setBlockState(new BlockPos(x + 3, y + 5, z + 3), woolState, 3);
		world.setBlockState(new BlockPos(x + 3, y + 5, z - 3), woolState, 3);
		world.setBlockState(new BlockPos(x + 4, y + 5, z + 1), woolState, 3);
		world.setBlockState(new BlockPos(x + 4, y + 5, z - 1), woolState, 3);
		world.setBlockState(new BlockPos(x - 1, y + 5, z + 4), woolState, 3);
		world.setBlockState(new BlockPos(x - 1, y + 5, z - 4), woolState, 3);
		world.setBlockState(new BlockPos(x - 3, y + 5, z + 3), woolState, 3);
		world.setBlockState(new BlockPos(x - 3, y + 5, z - 3), woolState, 3);
		world.setBlockState(new BlockPos(x - 4, y + 5, z + 1), woolState, 3);
		world.setBlockState(new BlockPos(x - 4, y + 5, z - 1), woolState, 3);
		world.setBlockState(new BlockPos(x + 1, y, z + 3), Blocks.iron_door.getStateFromMeta(3), 3);
		world.setBlockState(new BlockPos(x + 3, y, z - 1), Blocks.iron_door.getStateFromMeta(2), 3);
		world.setBlockState(new BlockPos(x - 1, y, z - 3), Blocks.iron_door.getStateFromMeta(1), 3);
		world.setBlockState(new BlockPos(x - 3, y, z + 1), Blocks.iron_door.getStateFromMeta(0), 3);
		world.setBlockState(new BlockPos(x + 1, y + 1, z + 3), Blocks.iron_door.getStateFromMeta(11), 3);
		world.setBlockState(new BlockPos(x + 3, y + 1, z - 1), Blocks.iron_door.getStateFromMeta(10), 3);
		world.setBlockState(new BlockPos(x - 1, y + 1, z - 3), Blocks.iron_door.getStateFromMeta(9), 3);
		world.setBlockState(new BlockPos(x - 3, y + 1, z + 1), Blocks.iron_door.getStateFromMeta(8), 3);
		world.setBlockState(new BlockPos(x + 2, y, z), Blocks.chest.getStateFromMeta(4), 3);
		TileEntityChest tileEntityChest = (TileEntityChest) world.getTileEntity(new BlockPos(x + 2, y, z));
		tileEntityChest.setInventorySlotContents(0, new ItemStack(Paintball.pod, 1, metadata));
		tileEntityChest.setInventorySlotContents(1, new ItemStack(Paintball.weaponRack, 1, metadata));
		tileEntityChest.setInventorySlotContents(2, new ItemStack(Paintball.gearRack, 1, metadata));
		tileEntityChest.setInventorySlotContents(3, new ItemStack(Paintball.medKit, 1, metadata));
		tileEntityChest.setInventorySlotContents(4, new ItemStack(Paintball.paintbrush, 1, metadata));
		tileEntityChest.setInventorySlotContents(13, new ItemStack(Paintball.flag, 1, metadata));
		tileEntityChest.setInventorySlotContents(22, new ItemStack(Paintball.pod, 1, metadata));
		tileEntityChest.setInventorySlotContents(23, new ItemStack(Paintball.weaponRack, 1, metadata));
		tileEntityChest.setInventorySlotContents(24, new ItemStack(Paintball.gearRack, 1, metadata));
		tileEntityChest.setInventorySlotContents(25, new ItemStack(Paintball.medKit, 1, metadata));
		tileEntityChest.setInventorySlotContents(26, new ItemStack(Paintball.paintbrush, 1, metadata));
		return;
	}

	@Override
	public int damageDropped(IBlockState state)
	{
		return ((EnumColor) state.getValue(BlockInstaBase.COLOR)).getMetadata();
	}

	@Override
	public void getSubBlocks(Item item, CreativeTabs creativeTabs, List list)
	{
		for (int i = 0; i < 6; i ++)
		{
			list.add(new ItemStack(item, 1, i));
		}
	}

	@Override
	public MapColor getMapColor(IBlockState state)
	{
		return ((EnumColor) state.getValue(BlockInstaBase.COLOR)).getMapColor();
	}

	@Override
	public IBlockState getStateFromMeta(int meta)
	{
		return this.getDefaultState().withProperty(BlockInstaBase.COLOR, EnumColor.byMetadata(meta));
	}

	@Override
	public int getMetaFromState(IBlockState state)
	{
		return ((EnumColor) state.getValue(BlockInstaBase.COLOR)).getMetadata();
	}

	@Override
	protected BlockState createBlockState()
	{
		return new BlockState(this, new IProperty[] {BlockInstaBase.COLOR});
	}
}
