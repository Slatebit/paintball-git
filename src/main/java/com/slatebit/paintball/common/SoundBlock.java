package com.slatebit.paintball.common;

import net.minecraft.block.Block;
import net.minecraft.block.Block.SoundType;

public class SoundBlock extends SoundType
{
	public SoundBlock(String name, float volume, float pitch)
	{
		super(name, volume, pitch);
	}

	@Override
	public String getBreakSound()
	{
		return "dig." + Block.soundTypeWood.soundName;
	}

	@Override
	public String getPlaceSound()
	{
		return this.soundName;
	}

	@Override
	public String getStepSound()
	{
		return "step." + Block.soundTypeWood.soundName;
	}
}