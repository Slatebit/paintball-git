package com.slatebit.paintball.common;

import java.util.ArrayList;
import java.util.List;
import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyEnum;
import net.minecraft.block.state.BlockState;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockPos;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.MathHelper;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BlockObject extends BlockContainer
{
	public static final PropertyEnum COLOR = PropertyEnum.create("color", EnumColor.class);
	private String type;

	public BlockObject(String type)
	{
		super(Material.wood);
		if (type != "Flag")
		{
			this.setHardness(2.5F);
		}
		else
		{
			this.setHardness(0F);
		}
		this.setCreativeTab(Paintball.TAB);
		GameRegistry.registerBlock(this, ItemMetadataBlock.class, type.toLowerCase());
		this.type = type;
		if (type == "Claymore")
		{
			this.setStepSound(new SoundBlock(Paintball.CLAYMORE_PLACE, 1, 1));
		}
		else if (type == "C4")
		{
			this.setStepSound(new SoundBlock(Paintball.C4_PLACE, 1, 1));
		}
	}

	@Override
	public TileEntity createNewTileEntity(World world, int i)
	{
		if (this.type == "Claymore")
		{
			return new TileEntityClaymore();
		}
		else if (this.type == "C4")
		{
			return new TileEntityC4();
		}
		else if (this.type == "Pod")
		{
			return new TileEntityPod();
		}
		else if (this.type == "Med Kit")
		{
			return new TileEntityMedKit();
		}
		else if (this.type == "Weapon Rack")
		{
			return new TileEntityWeaponRack();
		}
		else if (this.type == "Gear Rack")
		{
			return new TileEntityGearRack();
		}
		else if (this.type == "Flag")
		{
			return new TileEntityFlag();
		}
		return null;
	}

	@Override
	public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer entityPlayer, EnumFacing face, float xPosition, float yPosition, float zPosition)
	{
		InventoryPlayer inventory = entityPlayer.inventory;
		boolean isCreative = entityPlayer.capabilities.isCreativeMode;
		pos.getX();
		pos.getY();
		pos.getZ();
		int metadata = this.getMetaFromState(state);
		if (this.type == "Pod")
		{
			TileEntityPod tileEntityPod = (TileEntityPod) world.getTileEntity(pos);
			if (inventory != null && inventory.armorInventory != null && (inventory.getCurrentItem() == null || !(inventory.getCurrentItem().getItem() instanceof ItemPaintbrush)))
			{
				boolean isSameTeam = Paintball.isSameTeam(metadata, inventory.armorInventory);
				if (isSameTeam && (isCreative || tileEntityPod.cooldown == 0))
				{
					boolean add1 = inventory.addItemStackToInventory(new ItemStack(Paintball.pellet, 64, metadata));
					boolean add2 = inventory.addItemStackToInventory(new ItemStack(Paintball.pellet, 64, metadata));
					boolean add3 = inventory.addItemStackToInventory(new ItemStack(Paintball.pellet, 64, metadata));
					if (add1 || add2 || add3)
					{
						if (!isCreative)
						{
							tileEntityPod.cooldown = 1200;
						}
						world.playSoundAtEntity(entityPlayer, "random.pop", 1.0F, 1.0F / (world.rand.nextFloat() * 0.4F + 0.8F));
					}
				}
				else if (!world.isRemote && !isSameTeam)
				{
					entityPlayer.addChatMessage(new ChatComponentText("You aren't on the " + Paintball.COLOR_MAP.get(metadata).toLowerCase() + " team."));
				}
				return true;
			}
		}
		else if (this.type == "Med Kit")
		{
			TileEntityMedKit tileEntityMedKit = (TileEntityMedKit) world.getTileEntity(pos);
			if (inventory != null && inventory.armorInventory != null && (inventory.getCurrentItem() == null || !(inventory.getCurrentItem().getItem() instanceof ItemPaintbrush)))
			{
				boolean isSameTeam = Paintball.isSameTeam(metadata, inventory.armorInventory);
				if (isSameTeam && (isCreative || tileEntityMedKit.cooldown == 0 && entityPlayer.getHealth() < 20))
				{
					entityPlayer.heal(10);
					if (!isCreative)
					{
						tileEntityMedKit.cooldown = 1200;
					}
					world.playSoundAtEntity(entityPlayer, "random.pop", 1.0F, 1.0F / (world.rand.nextFloat() * 0.4F + 0.8F));
				}
				else if (!world.isRemote && !isSameTeam)
				{
					entityPlayer.addChatMessage(new ChatComponentText("You aren't on the " + Paintball.COLOR_MAP.get(metadata).toLowerCase() + " team."));
				}
				return true;
			}
		}
		else if (this.type == "Weapon Rack")
		{
			TileEntityWeaponRack tileEntityWeaponRack = (TileEntityWeaponRack) world.getTileEntity(pos);
			if (inventory != null && inventory.armorInventory != null && (inventory.getCurrentItem() == null || !(inventory.getCurrentItem().getItem() instanceof ItemPaintbrush)))
			{
				boolean isSameTeam = Paintball.isSameTeam(metadata, inventory.armorInventory);
				if (isSameTeam && (isCreative || tileEntityWeaponRack.cooldown == 0))
				{
					boolean add1 = entityPlayer.inventory.addItemStackToInventory(new ItemStack(Paintball.remote, 1, metadata));
					boolean add2 = entityPlayer.inventory.addItemStackToInventory(new ItemStack(Paintball.pistol, 1, metadata));
					boolean add3 = entityPlayer.inventory.addItemStackToInventory(new ItemStack(Paintball.shotgun, 1, metadata));
					boolean add4 = entityPlayer.inventory.addItemStackToInventory(new ItemStack(Paintball.rifle, 1, metadata));
					boolean add5 = entityPlayer.inventory.addItemStackToInventory(new ItemStack(Paintball.sniper, 1, metadata));
					boolean add6 = entityPlayer.inventory.addItemStackToInventory(new ItemStack(Paintball.launcher, 1, metadata));
					boolean add7 = entityPlayer.inventory.addItemStackToInventory(new ItemStack(Paintball.grenade, 2, metadata));
					boolean add8 = entityPlayer.inventory.addItemStackToInventory(new ItemStack(Paintball.claymore, 2, metadata));
					boolean add9 = entityPlayer.inventory.addItemStackToInventory(new ItemStack(Paintball.c4, 2, metadata));
					if (add1 || add2 || add3 || add4 || add5 || add6 || add7 || add8 || add9)
					{
						if (!isCreative)
						{
							tileEntityWeaponRack.cooldown = 1200;
						}
						world.playSoundAtEntity(entityPlayer, "random.pop", 1.0F, 1.0F / (world.rand.nextFloat() * 0.4F + 0.8F));
					}
				}
				else if (!world.isRemote && !isSameTeam)
				{
					entityPlayer.addChatMessage(new ChatComponentText("You aren't on the " + Paintball.COLOR_MAP.get(metadata).toLowerCase() + " team."));
				}
				return true;
			}
		}
		else if (this.type == "Gear Rack")
		{
			TileEntityGearRack tileEntityGearRack = (TileEntityGearRack) world.getTileEntity(pos);
			if (inventory != null && inventory.armorInventory != null)
			{
				if (inventory.getCurrentItem() == null || !(inventory.getCurrentItem().getItem() instanceof ItemPaintbrush))
				{
					if (tileEntityGearRack.cooldown == 0)
					{
						Item[] armor = new Item[] {Paintball.shoes, Paintball.pants, Paintball.chest, Paintball.helmet};
						boolean[] success = new boolean[4];
						for (int i = 3; i > -1; i --)
						{
							ItemStack itemStack = new ItemStack(armor[i], 1, metadata);
							if (inventory.armorItemInSlot(i) == null)
							{
								entityPlayer.setCurrentItemOrArmor(i + 1, itemStack);
								success[i] = true;
							}
							else
							{
								success[i] = entityPlayer.inventory.addItemStackToInventory(itemStack);
							}
						}
						if (success[0] == true || success[1] == true || success[2] == true || success[3] == true)
						{
							if (!isCreative)
							{
								tileEntityGearRack.cooldown = 1200;
							}
							world.playSoundAtEntity(entityPlayer, "random.pop", 1.0F, 1.0F / (world.rand.nextFloat() * 0.4F + 0.8F));
						}
					}
					return true;
				}
			}
		}
		else if (this.type == "Flag")
		{
			if (!isCreative)
			{
				world.setBlockToAir(pos);
			}
			this.dropBlockAsItem(world, pos, state, 0);
			return true;
		}
		return false;
	}

	@Override
	public void onBlockPlacedBy(World world, BlockPos pos, IBlockState state, EntityLivingBase entityLivingBase, ItemStack itemStack)
	{
		TileEntityObject tileEntityObject = (TileEntityObject) world.getTileEntity(pos);
		if (this.type == "Claymore")
		{
			tileEntityObject.direction = MathHelper.floor_double((entityLivingBase.rotationYaw + 180) * 16 / 360 + 0.5) & 15;
			tileEntityObject.owner = (EntityPlayer) entityLivingBase;
		}
		else
		{
			tileEntityObject.direction = MathHelper.floor_double(entityLivingBase.rotationYaw * 4 / 360 + 0.5) & 3;
		}
	}

	@Override
	public void setBlockBoundsBasedOnState(IBlockAccess iBlockAccess, BlockPos pos)
	{
		if (this.type == "Claymore")
		{
			this.setBlockBounds(0.25F, 0.0F, 0.25F, 0.75F, 0.5F, 0.75F);
		}
		else if (this.type == "C4")
		{
			this.setBlockBounds(0.25F, 0.0F, 0.25F, 0.75F, 0.25F, 0.75F);
		}
		else if (this.type == "Pod")
		{
			this.setBlockBounds(0.34375F, 0.0F, 0.34375F, 0.65625F, 0.5625F, 0.65625F);
		}
		else if (this.type == "Med Kit")
		{
			this.setBlockBounds(0.125F, 0.0F, 0.125F, 0.875F, 0.6875F, 0.875F);
		}
	}

	@Override
	public void onBlockClicked(World world, BlockPos pos, EntityPlayer entityPlayer)
	{
		IBlockState state = world.getBlockState(pos);
		Block block = state.getBlock();
		if (block == Paintball.c4)
		{
			TileEntityC4 tileEntityC4 = (TileEntityC4) world.getTileEntity(pos);
			InventoryPlayer inventory = entityPlayer.inventory;
			if (inventory != null && inventory.armorInventory != null)
			{
				ItemStack itemStack = inventory.getCurrentItem();
				int color = block.getMetaFromState(state);
				boolean isSameTeam = Paintball.isSameTeam(itemStack.getItemDamage(), entityPlayer.inventory.armorInventory);
				if (!world.isRemote)
				{
					if (isSameTeam && color == itemStack.getItemDamage() && (tileEntityC4.owner == null || tileEntityC4.owner == entityPlayer))
					{
						String key = entityPlayer.getDisplayName() + " " + itemStack.getItemDamage();
						ArrayList<TileEntity> tileEntitiesC4 = Paintball.C4_REMOTE.get(key);
						if (tileEntitiesC4 == null)
						{
							tileEntitiesC4 = new ArrayList<TileEntity>();
							Paintball.C4_REMOTE.put(key, tileEntitiesC4);
						}
						if (!tileEntitiesC4.contains(tileEntityC4))
						{
							tileEntitiesC4.add(tileEntityC4);
							tileEntityC4.owner = entityPlayer;
							entityPlayer.addChatMessage(new ChatComponentText("The C4 is now connected to the remote."));
						}
						else if (tileEntitiesC4.contains(tileEntityC4))
						{
							tileEntitiesC4.remove(tileEntityC4);
							tileEntityC4.owner = null;
							tileEntitiesC4 = null;
							entityPlayer.addChatMessage(new ChatComponentText("The C4 is no longer connected to the remote."));
						}
					}
					else if (isSameTeam && tileEntityC4.owner != null && tileEntityC4.owner != entityPlayer)
					{
						entityPlayer.addChatMessage(new ChatComponentText("The C4 is already connected to another remote."));
					}
					else
					{
						entityPlayer.addChatMessage(new ChatComponentText("You aren't on the " + Paintball.COLOR_MAP.get(color).toLowerCase() + " team."));
					}
				}
			}
		}
	}

	@Override
	public void onNeighborBlockChange(World world, BlockPos pos, IBlockState state, Block neighbor)
	{
		int x = pos.getX();
		int y = pos.getY();
		int z = pos.getZ();
		if (!world.getBlockState(new BlockPos(x, y - 1, z)).getBlock().getMaterial().isSolid())
		{
			this.dropBlockAsItem(world, pos, state, 0);
			world.setBlockToAir(pos);
		}
	}

	@Override
	public boolean canPlaceBlockAt(World world, BlockPos pos)
	{
		int x = pos.getX();
		int y = pos.getY();
		int z = pos.getZ();
		return World.doesBlockHaveSolidTopSurface(world, new BlockPos(x, y - 1, z));
	}

	@Override
	@SideOnly(Side.CLIENT)
	public boolean addDestroyEffects(World world, BlockPos pos, net.minecraft.client.particle.EffectRenderer effectRenderer)
	{
		int color;
		IBlockState state = world.getBlockState(pos);
		switch ((EnumColor) state.getValue(BlockObject.COLOR))
		{
		case RED:
			color = 14;
		break;
		case ORANGE:
			color = 1;
		break;
		case YELLOW:
			color = 4;
		break;
		case GREEN:
			color = 13;
		break;
		case BLUE:
		default:
			color = 11;
		break;
		case PURPLE:
			color = 10;
		}
		effectRenderer.func_180533_a(pos, Blocks.wool.getStateFromMeta(color));
		return true;
	}

	@Override
	public boolean isOpaqueCube()
	{
		return false;
	}

	@Override
	public int damageDropped(IBlockState state)
	{
		return ((EnumColor) state.getValue(BlockObject.COLOR)).getMetadata();
	}

	@Override
	public void getSubBlocks(Item item, CreativeTabs creativeTabs, List list)
	{
		for (int i = 0; i < 6; i ++)
		{
			list.add(new ItemStack(item, 1, i));
		}
	}

	@Override
	public MapColor getMapColor(IBlockState state)
	{
		return ((EnumColor) state.getValue(BlockObject.COLOR)).getMapColor();
	}

	@Override
	public IBlockState getStateFromMeta(int meta)
	{
		return this.getDefaultState().withProperty(BlockObject.COLOR, EnumColor.byMetadata(meta));
	}

	@Override
	public int getMetaFromState(IBlockState state)
	{
		return ((EnumColor) state.getValue(BlockObject.COLOR)).getMetadata();
	}

	@Override
	protected BlockState createBlockState()
	{
		return new BlockState(this, new IProperty[] {BlockObject.COLOR});
	}
}
