package com.slatebit.paintball.common;

import java.util.List;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EntityDamageSource;
import net.minecraft.world.World;

public class TileEntityClaymore extends TileEntityObject
{
	private int rotation;
	private boolean triggered;

	@Override
	public void update()
	{
		this.rotation = this.direction * 360 / 16 + 180;
		if (this.rotation >= 360)
		{
			this.rotation -= 360;
		}
		World world = this.worldObj;
		int x = this.pos.getX();
		int y = this.pos.getY();
		int z = this.pos.getZ();
		List list = world.getEntitiesWithinAABBExcludingEntity(null, AxisAlignedBB.fromBounds(x + 0.5 - 3.5, y, z + 0.5 - 3.5, x + 0.5 + 3.5, y + 3.5, z + 0.5 + 3.5));
		for (int i = 0; i < list.size(); ++ i)
		{
			Entity entity = (Entity) list.get(i);
			if (this.canTrigger(entity))
			{
				DamageSource damageSource = new EntityDamageSource("Paintball", this.owner);
				if (entity instanceof EntityPlayer)
				{
					EntityPlayer entityPlayer = (EntityPlayer) entity;
					InventoryPlayer inventory = entityPlayer.inventory;
					if (!entityPlayer.capabilities.isCreativeMode && entityPlayer.inventory != null && inventory.armorInventory != null)
					{
						if (!Paintball.isSameTeam(this.getBlockMetadata(), entityPlayer.inventory.armorInventory))
						{
							if (this.owner != null)
							{
								Paintball.NETWORK_HELPER.sendPacketToPlayer(new PacketHandler(this.owner.getDisplayNameString(), 2), this.owner);
							}
							entityPlayer.hurtResistantTime = 0;
							entityPlayer.attackEntityFrom(damageSource, 8);
							this.triggered = true;
						}
					}
				}
				else if (entity instanceof EntityLiving)
				{
					if (this.owner != null)
					{
						Paintball.NETWORK_HELPER.sendPacketToPlayer(new PacketHandler(this.owner.getDisplayNameString(), 2), this.owner);
					}
					EntityLiving entityLiving = (EntityLiving) entity;
					entityLiving.hurtResistantTime = 0;
					entityLiving.attackEntityFrom(damageSource, 8);
					this.triggered = true;
				}
			}
		}
		if (this.triggered)
		{
			world.createExplosion(null, this.pos.getX(), this.pos.getY(), this.pos.getZ(), 0.0F, true);
			world.setBlockToAir(this.pos);
		}
	}

	private boolean canTrigger(Entity entity)
	{
		double rotation = Math.toRadians(this.rotation);
		double xDifference = entity.posX - this.pos.getX() - 0.5;
		double zDifference = entity.posZ - this.pos.getZ() - 0.5;
		double distance = Math.sqrt(Math.pow(xDifference, 2) + Math.pow(zDifference, 2));
		double dotProduct = xDifference * -Math.sin(rotation) + zDifference * Math.cos(rotation);
		double angle = Math.acos(dotProduct / distance);
		if (angle <= 0.91625)
		{
			return true;
		}
		return false;
	}
}
