package com.slatebit.paintball.common;

import java.util.List;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EntityDamageSource;
import net.minecraft.world.World;

public class TileEntityC4 extends TileEntityObject
{
	public boolean triggered;

	@Override
	public void update()
	{
		if (this.triggered)
		{
			World world = this.worldObj;
			int x = this.pos.getX();
			int y = this.pos.getY();
			int z = this.pos.getZ();
			List list = world.getEntitiesWithinAABBExcludingEntity(null, AxisAlignedBB.fromBounds(x + 0.5 - 3.5, y, z + 0.5 - 3.5, x + 0.5 + 3.5, y + 3.5, z + 0.5 + 3.5));
			for (int i = 0; i < list.size(); ++ i)
			{
				DamageSource damageSource = new EntityDamageSource("Paintball", this.owner);
				Entity entity = (Entity) list.get(i);
				if (entity instanceof EntityPlayer)
				{
					EntityPlayer entityPlayer = (EntityPlayer) entity;
					InventoryPlayer inventory = entityPlayer.inventory;
					if (!entityPlayer.capabilities.isCreativeMode && entityPlayer.inventory != null && inventory.armorInventory != null)
					{
						if (!Paintball.isSameTeam(this.getBlockMetadata(), entityPlayer.inventory.armorInventory))
						{
							if (this.owner != null)
							{
								Paintball.NETWORK_HELPER.sendPacketToPlayer(new PacketHandler(this.owner.getDisplayNameString(), 2), this.owner);
							}
							entityPlayer.hurtResistantTime = 0;
							entityPlayer.attackEntityFrom(damageSource, 8);
						}
					}
				}
				else if (entity instanceof EntityLiving)
				{
					if (this.owner != null)
					{
						Paintball.NETWORK_HELPER.sendPacketToPlayer(new PacketHandler(this.owner.getDisplayNameString(), 2), this.owner);
					}
					EntityLiving entityLiving = (EntityLiving) entity;
					entityLiving.hurtResistantTime = 0;
					entityLiving.attackEntityFrom(damageSource, 8);
				}
			}
			world.createExplosion(null, this.pos.getX(), this.pos.getY(), this.pos.getZ(), 0.0F, true);
			world.setBlockToAir(this.pos);
		}
	}
}
