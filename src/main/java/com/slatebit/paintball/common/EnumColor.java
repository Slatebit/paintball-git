package com.slatebit.paintball.common;

import net.minecraft.block.material.MapColor;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.IStringSerializable;

public enum EnumColor implements IStringSerializable
{
	RED(0, 1, "red", "red", MapColor.redColor, EnumChatFormatting.DARK_RED), ORANGE(1, 14, "orange", "orange", MapColor.adobeColor, EnumChatFormatting.GOLD), YELLOW(2, 11, "yellow", "yellow", MapColor.yellowColor, EnumChatFormatting.YELLOW), GREEN(3, 2, "green", "green", MapColor.greenColor, EnumChatFormatting.DARK_GREEN), BLUE(4, 4, "blue", "blue", MapColor.blueColor, EnumChatFormatting.DARK_BLUE), PURPLE(5, 5, "purple", "purple", MapColor.purpleColor, EnumChatFormatting.DARK_PURPLE);
	private static final EnumColor[] META_LOOKUP = new EnumColor[EnumColor.values().length];
	private final int meta;
	private final int dyeDamage;
	private final String name;
	private final String unlocalizedName;
	private final MapColor mapColor;

	private EnumColor(int meta, int dyeDamage, String name, String unlocalizedName, MapColor mapColorIn, EnumChatFormatting chatColor)
	{
		this.meta = meta;
		this.dyeDamage = dyeDamage;
		this.name = name;
		this.unlocalizedName = unlocalizedName;
		this.mapColor = mapColorIn;
	}

	public int getMetadata()
	{
		return this.meta;
	}

	public int getDyeDamage()
	{
		return this.dyeDamage;
	}

	public String getUnlocalizedName()
	{
		return this.unlocalizedName;
	}

	public MapColor getMapColor()
	{
		return this.mapColor;
	}

	public static EnumColor byMetadata(int meta)
	{
		if (meta < 0 || meta >= EnumColor.META_LOOKUP.length)
		{
			meta = 0;
		}
		return EnumColor.META_LOOKUP[meta];
	}

	@Override
	public String toString()
	{
		return this.unlocalizedName;
	}

	@Override
	public String getName()
	{
		return this.name;
	}

	static
	{
		EnumColor[] var0 = EnumColor.values();
		int var1 = var0.length;
		for (int var2 = 0; var2 < var1; ++ var2)
		{
			EnumColor var3 = var0[var2];
			EnumColor.META_LOOKUP[var3.getMetadata()] = var3;
		}
	}
}