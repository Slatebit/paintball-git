package com.slatebit.paintball.common;

import java.util.ArrayList;
import java.util.List;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockPos;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ItemPaintbrush extends Item
{
	public ItemPaintbrush()
	{
		super();
		this.setHasSubtypes(true);
		this.setMaxStackSize(1);
		this.setCreativeTab(Paintball.TAB);
		GameRegistry.registerItem(this, "paintbrush");
	}

	@Override
	public boolean onItemUse(ItemStack itemStack, EntityPlayer entityPlayer, World world, BlockPos pos, EnumFacing side, float xHit, float yHit, float zHit)
	{
		IBlockState state = world.getBlockState(pos);
		Block block = state.getBlock();
		int color = itemStack.getItemDamage();
		int metadata = block.getMetaFromState(state);
		int woolColor = Paintball.WOOL_MAP.get(Paintball.COLOR_MAP.get(color));
		if (block == Blocks.wool && metadata != woolColor)
		{
			world.setBlockState(pos, Blocks.wool.getStateFromMeta(woolColor), 3);
		}
		else if (block == Blocks.glass)
		{
			world.setBlockState(pos, Blocks.stained_glass.getStateFromMeta(woolColor), 3);
		}
		else if (block == Blocks.glass_pane)
		{
			world.setBlockState(pos, Blocks.stained_glass_pane.getStateFromMeta(woolColor), 3);
		}
		else if (block == Blocks.stained_glass && metadata != woolColor)
		{
			world.setBlockState(pos, Blocks.stained_glass.getStateFromMeta(woolColor), 3);
		}
		else if (block == Blocks.stained_glass_pane && metadata != woolColor)
		{
			world.setBlockState(pos, Blocks.stained_glass_pane.getStateFromMeta(woolColor), 3);
		}
		else if (block == Blocks.stained_hardened_clay && metadata != woolColor)
		{
			world.setBlockState(pos, Blocks.stained_hardened_clay.getStateFromMeta(woolColor), 3);
		}
		else if (block == Paintball.scanner)
		{
			int color2 = (metadata & 7) - 1;
			if (color2 < 0)
			{
				color2 = 5;
			}
			if (color2 != color)
			{
				world.setBlockState(pos, Paintball.scanner.getStateFromMeta(color), 3);
			}
		}
		else if (block == Paintball.c4 && metadata != color)
		{
			int direction = 0;
			TileEntityC4 tileEntityC4 = (TileEntityC4) world.getTileEntity(pos);
			direction = tileEntityC4.direction;
			if (!world.isRemote)
			{
				if (tileEntityC4.owner != null)
				{
					String key = tileEntityC4.owner.getDisplayName() + " " + metadata;
					ArrayList<TileEntity> tileEntitiesC4 = Paintball.C4_REMOTE.get(key);
					if (tileEntitiesC4 != null && tileEntitiesC4.contains(tileEntityC4))
					{
						tileEntitiesC4.remove(tileEntityC4);
						tileEntitiesC4 = null;
						if (tileEntityC4.owner != entityPlayer)
						{
							entityPlayer.addChatMessage(new ChatComponentText("Your C4 has been hijacked, so it is no longer connected to your " + Paintball.COLOR_MAP.get(metadata).toLowerCase() + " remote."));
						}
						else
						{
							entityPlayer.addChatMessage(new ChatComponentText("You painted your C4, so it is no longer connected to your " + Paintball.COLOR_MAP.get(metadata).toLowerCase() + " remote."));
						}
					}
					else if (tileEntityC4.owner != entityPlayer)
					{
						entityPlayer.addChatMessage(new ChatComponentText("Your C4 has been hijacked."));
					}
					tileEntityC4.owner = null;
				}
			}
			world.setBlockState(pos, Paintball.c4.getStateFromMeta(color), 3);
			((TileEntityObject) world.getTileEntity(pos)).direction = direction;
		}
		else if ((block == Paintball.claymore || block == Paintball.flag || block == Paintball.pod || block == Paintball.weaponRack || block == Paintball.gearRack || block == Paintball.medKit) && metadata != color)
		{
			int direction = ((TileEntityObject) world.getTileEntity(pos)).direction;
			world.setBlockState(pos, block.getStateFromMeta(color), 3);
			((TileEntityObject) world.getTileEntity(pos)).direction = direction;
		}
		return true;
	}

	public EntityPlayer findPlayer(EntityLiving entityLiving)
	{
		EntityPlayer entityPlayer = entityLiving.worldObj.getClosestPlayerToEntity(entityLiving, 16.0D);
		return entityPlayer != null ? entityPlayer : null;
	}

	@Override
	public void getSubItems(Item item, CreativeTabs creativeTabs, List list)
	{
		for (int i = 0; i < 6; i ++)
		{
			list.add(new ItemStack(item, 1, i));
		}
	}

	@Override
	@SideOnly(Side.CLIENT)
	public String getItemStackDisplayName(ItemStack itemStack)
	{
		return "Paintbrush (" + Paintball.COLOR_MAP.get(itemStack.getItemDamage()).charAt(0) + ")";
	}
}