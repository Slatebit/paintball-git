package com.slatebit.paintball.common;

import java.util.List;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class CreativeTab extends CreativeTabs
{
	CreativeTab(int tabIndex, String tabLabel)
	{
		super(tabIndex, tabLabel);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void displayAllReleventItems(List list)
	{
		for (int i = 0; i < 6; i ++)
		{
			list.add(new ItemStack(Paintball.scanner, 1, i));
			list.add(new ItemStack(Paintball.instaBase, 1, i));
			list.add(new ItemStack(Paintball.helmet, 1, i));
			list.add(new ItemStack(Paintball.chest, 1, i));
			list.add(new ItemStack(Paintball.pants, 1, i));
			list.add(new ItemStack(Paintball.shoes, 1, i));
			list.add(new ItemStack(Paintball.remote, 1, i));
			list.add(new ItemStack(Paintball.paintbrush, 1, i));
			list.add(new ItemStack(Paintball.pellet, 1, i));
			list.add(new ItemStack(Paintball.pistol, 1, i));
			list.add(new ItemStack(Paintball.shotgun, 1, i));
			list.add(new ItemStack(Paintball.rifle, 1, i));
			list.add(new ItemStack(Paintball.sniper, 1, i));
			list.add(new ItemStack(Paintball.launcher, 1, i));
			list.add(new ItemStack(Paintball.grenade, 1, i));
			list.add(new ItemStack(Paintball.claymore, 1, i));
			list.add(new ItemStack(Paintball.c4, 1, i));
			list.add(new ItemStack(Paintball.flag, 1, i));
			list.add(new ItemStack(Paintball.pod, 1, i));
			list.add(new ItemStack(Paintball.weaponRack, 1, i));
			list.add(new ItemStack(Paintball.gearRack, 1, i));
			list.add(new ItemStack(Paintball.medKit, 1, i));
		}
	}

	@Override
	@SideOnly(Side.CLIENT)
	public Item getTabIconItem()
	{
		return Paintball.rifle;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public String getTranslatedTabLabel()
	{
		return this.getTabLabel();
	}
}