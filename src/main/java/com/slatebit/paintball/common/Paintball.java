package com.slatebit.paintball.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import net.minecraft.block.Block;
import net.minecraft.block.BlockDispenser;
import net.minecraft.block.BlockPressurePlate;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor.ArmorMaterial;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.registry.EntityRegistry;
import net.minecraftforge.fml.common.registry.GameRegistry;

@Mod(modid = "Paintball", name = "Paintball", version = "1.8.1v1")
public class Paintball
{
	@SidedProxy(clientSide = "com.slatebit.paintball.minecraft.ClientProxy", serverSide = "com.slatebit.paintball.common.CommonProxy")
	public static CommonProxy PROXY = new CommonProxy();
	public static final NetworkHelper NETWORK_HELPER = new NetworkHelper("Paintball", PacketHandler.class);
	public static final ArmorMaterial GEAR_MATERIAL = EnumHelper.addArmorMaterial("Paintball", "", 1, new int[] {0, 0, 0, 0}, 0);
	public static final Map<Integer, String> ARMOR_TYPE_MAP = new HashMap<Integer, String>();
	public static final Map<String, ArrayList<TileEntity>> C4_REMOTE = new HashMap<String, ArrayList<TileEntity>>();
	public static final Map<Integer, String> COLOR_MAP = new HashMap<Integer, String>();
	public static final Map<String, Integer> DYE_MAP = new HashMap<String, Integer>();
	public static final Map<String, Integer> WOOL_MAP = new HashMap<String, Integer>();
	public static final CreativeTabs TAB = new CreativeTab(CreativeTabs.getNextID(), "Paintball");
	public static String GUN_SHOOT = "paintball:Gun Shoot";
	public static String GUN_SPLAT = "paintball:Gun Splat";
	public static String GRENADE_PIN = "paintball:Grenade Pin";
	public static String GRENADE_LAND = "paintball:Grenade Land";
	public static String CLAYMORE_PLACE = "paintball:Claymore Place";
	public static String C4_PLACE = "paintball:C4 Place";
	public static int armorRender;
	public static int hitMarker = 0;
	public static int shootTime = 0;
	public static float zoomLevel = 1.0F;
	public static float mouseSensitivity;
	public static Block scanner;
	public static Block instaBase;
	public static Item helmet;
	public static Item chest;
	public static Item pants;
	public static Item shoes;
	public static Item remote;
	public static Item paintbrush;
	public static Item pellet;
	public static Item pistol;
	public static Item shotgun;
	public static Item rifle;
	public static Item sniper;
	public static Item launcher;
	public static Item grenade;
	public static Block claymore;
	public static Block c4;
	public static Block flag;
	public static Block pod;
	public static Block weaponRack;
	public static Block gearRack;
	public static Block medKit;

	@EventHandler
	public void preInitialization(FMLPreInitializationEvent event)
	{
		Paintball.ARMOR_TYPE_MAP.put(0, "Helmet");
		Paintball.ARMOR_TYPE_MAP.put(1, "Chest");
		Paintball.ARMOR_TYPE_MAP.put(2, "Pants");
		Paintball.ARMOR_TYPE_MAP.put(3, "Shoes");
		Paintball.COLOR_MAP.put(0, "Red");
		Paintball.COLOR_MAP.put(1, "Orange");
		Paintball.COLOR_MAP.put(2, "Yellow");
		Paintball.COLOR_MAP.put(3, "Green");
		Paintball.COLOR_MAP.put(4, "Blue");
		Paintball.COLOR_MAP.put(5, "Purple");
		Paintball.DYE_MAP.put("Red", 1);
		Paintball.DYE_MAP.put("Orange", 14);
		Paintball.DYE_MAP.put("Yellow", 11);
		Paintball.DYE_MAP.put("Green", 2);
		Paintball.DYE_MAP.put("Blue", 4);
		Paintball.DYE_MAP.put("Purple", 5);
		Paintball.WOOL_MAP.put("Red", 14);
		Paintball.WOOL_MAP.put("Orange", 1);
		Paintball.WOOL_MAP.put("Yellow", 4);
		Paintball.WOOL_MAP.put("Green", 13);
		Paintball.WOOL_MAP.put("Blue", 11);
		Paintball.WOOL_MAP.put("Purple", 10);
		FMLCommonHandler.instance().bus().register(new EventHook());
		MinecraftForge.EVENT_BUS.register(new EventHook());
		Paintball.scanner = new BlockScanner(Material.cloth, BlockPressurePlate.Sensitivity.MOBS);
		Paintball.instaBase = new BlockInstaBase();
		Paintball.helmet = new ItemGear(0);
		Paintball.chest = new ItemGear(1);
		Paintball.pants = new ItemGear(2);
		Paintball.shoes = new ItemGear(3);
		Paintball.remote = new ItemRemote();
		Paintball.paintbrush = new ItemPaintbrush();
		Paintball.pellet = new ItemPellet();
		Paintball.pistol = new ItemGun(4, 10, 30, "Pistol");
		Paintball.shotgun = new ItemGun(1, 15, 15, "Shotgun");
		Paintball.rifle = new ItemGun(2, 5, 45, "Rifle");
		Paintball.sniper = new ItemGun(8, 20, 60, "Sniper");
		Paintball.launcher = new ItemGun(0, 20, 0, "Launcher");
		Paintball.grenade = new ItemGrenade();
		Paintball.claymore = new BlockObject("Claymore");
		Paintball.c4 = new BlockObject("C4");
		Paintball.flag = new BlockObject("Flag");
		Paintball.pod = new BlockObject("Pod");
		Paintball.weaponRack = new BlockObject("Weapon Rack");
		Paintball.gearRack = new BlockObject("Gear Rack");
		Paintball.medKit = new BlockObject("Med Kit");
		for (int i = 0; i < 6; i ++)
		{
			String color = Paintball.COLOR_MAP.get(i);
			ItemStack helmetItemStack = new ItemStack(Paintball.helmet, 1, i);
			ItemStack chestItemStack = new ItemStack(Paintball.chest, 1, i);
			ItemStack pantsItemStack = new ItemStack(Paintball.pants, 1, i);
			ItemStack shoesItemStack = new ItemStack(Paintball.shoes, 1, i);
			ItemStack pistolItemStack = new ItemStack(Paintball.pistol, 1, i);
			ItemStack shotgunItemStack = new ItemStack(Paintball.shotgun, 1, i);
			ItemStack rifleItemStack = new ItemStack(Paintball.rifle, 1, i);
			ItemStack sniperItemStack = new ItemStack(Paintball.sniper, 1, i);
			ItemStack launcherItemStack = new ItemStack(Paintball.launcher, 1, i);
			ItemStack grenadeItemStack = new ItemStack(Paintball.grenade, 1, i);
			ItemStack claymoreItemStack = new ItemStack(Paintball.claymore, 1, i);
			ItemStack c4ItemStack = new ItemStack(Paintball.c4, 1, i);
			GameRegistry.addRecipe(new ItemStack(Paintball.scanner, 1, i), new Object[] {"Z", 'Z', new ItemStack(Blocks.wool, 2, Paintball.WOOL_MAP.get(color))});
			GameRegistry.addRecipe(new ItemStack(Paintball.instaBase, 1, i), new Object[] {"YZY", "YXY", "YWY", 'W', Items.iron_door, 'X', Items.diamond, 'Y', new ItemStack(Blocks.wool, 2, Paintball.WOOL_MAP.get(color)), 'Z', Blocks.chest});
			GameRegistry.addRecipe(helmetItemStack, new Object[] {"ZZZ", "Z Z", 'Z', new ItemStack(Blocks.wool, 2, Paintball.WOOL_MAP.get(color))});
			GameRegistry.addRecipe(chestItemStack, new Object[] {"Z Z", "ZZZ", "ZZZ", 'Z', new ItemStack(Blocks.wool, 2, Paintball.WOOL_MAP.get(color))});
			GameRegistry.addRecipe(pantsItemStack, new Object[] {"ZZZ", "Z Z", "Z Z", 'Z', new ItemStack(Blocks.wool, 2, Paintball.WOOL_MAP.get(color))});
			GameRegistry.addRecipe(shoesItemStack, new Object[] {"Z Z", "Z Z", 'Z', new ItemStack(Blocks.wool, 2, Paintball.WOOL_MAP.get(color))});
			GameRegistry.addRecipe(new ItemStack(Paintball.remote, 1, i), new Object[] {"WXW", "YZY", "WYW", 'W', new ItemStack(Items.dye, 2, Paintball.DYE_MAP.get(color)), 'X', Blocks.redstone_torch, 'Y', Items.iron_ingot, 'Z', Blocks.stone_button});
			GameRegistry.addRecipe(new ItemStack(Paintball.paintbrush, 1, i), new Object[] {"Y  ", " Z ", "  Z", 'Y', new ItemStack(Blocks.wool, 2, Paintball.WOOL_MAP.get(color)), 'Z', Items.stick});
			GameRegistry.addRecipe(new ItemStack(Paintball.pellet, 32, i), new Object[] {" Z ", "ZYZ", " Z ", 'Y', new ItemStack(Items.dye, 2, Paintball.DYE_MAP.get(color)), 'Z', Blocks.glass});
			GameRegistry.addRecipe(pistolItemStack, new Object[] {"   ", " X ", "YZ ", 'X', Items.iron_ingot, 'Y', new ItemStack(Items.dye, 2, Paintball.DYE_MAP.get(color)), 'Z', Items.stick});
			GameRegistry.addRecipe(shotgunItemStack, new Object[] {"   ", " X ", "YZX", 'X', Items.iron_ingot, 'Y', new ItemStack(Items.dye, 2, Paintball.DYE_MAP.get(color)), 'Z', Items.stick});
			GameRegistry.addRecipe(rifleItemStack, new Object[] {"X  ", " X ", "YZX", 'X', Items.iron_ingot, 'Y', new ItemStack(Items.dye, 2, Paintball.DYE_MAP.get(color)), 'Z', Items.stick});
			GameRegistry.addRecipe(sniperItemStack, new Object[] {"X  ", " XW", "YZX", 'W', Blocks.glass, 'X', Items.iron_ingot, 'Y', new ItemStack(Items.dye, 2, Paintball.DYE_MAP.get(color)), 'Z', Items.stick});
			GameRegistry.addRecipe(launcherItemStack, new Object[] {"X  ", "WX ", "YZX", 'W', Items.gunpowder, 'X', Items.iron_ingot, 'Y', new ItemStack(Items.dye, 2, Paintball.DYE_MAP.get(color)), 'Z', Items.stick});
			GameRegistry.addRecipe(grenadeItemStack, new Object[] {" Y ", "YZY", " Y ", 'Y', Items.iron_ingot, 'Z', new ItemStack(Items.dye, 2, Paintball.DYE_MAP.get(color))});
			GameRegistry.addRecipe(claymoreItemStack, new Object[] {" X ", "XYX", "ZXZ", 'X', Items.iron_ingot, 'Y', new ItemStack(Items.dye, 2, Paintball.DYE_MAP.get(color)), 'Z', Items.stick});
			GameRegistry.addRecipe(c4ItemStack, new Object[] {"ZX ", "XYX", " X ", 'X', Items.iron_ingot, 'Y', new ItemStack(Items.dye, 2, Paintball.DYE_MAP.get(color)), 'Z', Blocks.redstone_torch});
			GameRegistry.addRecipe(new ItemStack(Paintball.flag, 1, i), new Object[] {" YX", " Y ", "ZZZ", 'X', new ItemStack(Blocks.wool, 2, Paintball.WOOL_MAP.get(color)), 'Y', Items.stick, 'Z', Blocks.stone});
			GameRegistry.addRecipe(new ItemStack(Paintball.pod, 1, i), new Object[] {"XYX", "YZY", "XYX", 'X', new ItemStack(Items.dye, 2, Paintball.DYE_MAP.get(color)), 'Y', Blocks.glass, 'Z', Items.diamond});
			GameRegistry.addRecipe(new ItemStack(Paintball.weaponRack, 1, i), new Object[] {"RST", "UZV", "WXY", 'R', pistolItemStack, 'S', shotgunItemStack, 'T', rifleItemStack, 'U', sniperItemStack, 'V', launcherItemStack, 'W', grenadeItemStack, 'X', claymoreItemStack, 'Y', c4ItemStack, 'Z', Items.diamond});
			GameRegistry.addRecipe(new ItemStack(Paintball.gearRack, 1, i), new Object[] {"VUW", "UZU", "XUY", 'U', new ItemStack(Blocks.wool, 2, Paintball.WOOL_MAP.get(color)), 'V', helmetItemStack, 'W', chestItemStack, 'X', pantsItemStack, 'Y', shoesItemStack, 'Z', Items.diamond});
			GameRegistry.addRecipe(new ItemStack(Paintball.medKit, 1, i), new Object[] {" Y ", "YZY", "YYY", 'Y', new ItemStack(Blocks.wool, 2, Paintball.WOOL_MAP.get(color)), 'Z', Items.golden_apple});
		}
	}

	@EventHandler
	public void initialization(FMLInitializationEvent event)
	{
		Paintball.PROXY.addRenders();
		EntityRegistry.registerModEntity(EntityPellet.class, "Pellet", 0, this, 50, 1, true);
		EntityRegistry.registerModEntity(EntityGrenade.class, "Grenade", 1, this, 50, 1, true);
		GameRegistry.registerTileEntity(TileEntityClaymore.class, "Claymore");
		GameRegistry.registerTileEntity(TileEntityC4.class, "C4");
		GameRegistry.registerTileEntity(TileEntityFlag.class, "Flag");
		GameRegistry.registerTileEntity(TileEntityPod.class, "Pod");
		GameRegistry.registerTileEntity(TileEntityWeaponRack.class, "Weapon Rack");
		GameRegistry.registerTileEntity(TileEntityGearRack.class, "Gear Rack");
		GameRegistry.registerTileEntity(TileEntityMedKit.class, "Med Kit");
		BlockDispenser.dispenseBehaviorRegistry.putObject(Paintball.pellet, new DispenseProjectile(0));
		BlockDispenser.dispenseBehaviorRegistry.putObject(Paintball.grenade, new DispenseProjectile(1));
	}

	public static boolean consumeInventoryItemStack(InventoryPlayer inventory, ItemStack itemStack, int quantity)
	{
		for (int i = 0; i < 36; i ++)
		{
			ItemStack itemStack2 = inventory.getStackInSlot(i);
			if (itemStack2 != null && itemStack2.getItem() == itemStack.getItem() && itemStack2.getItemDamage() == itemStack.getItemDamage())
			{
				inventory.decrStackSize(i, quantity);
				return true;
			}
		}
		return false;
	}

	public static boolean isSameTeam(int color, ItemStack[] armorInventory)
	{
		if (armorInventory[3] != null && armorInventory[2] != null && armorInventory[1] != null && armorInventory[0] != null)
		{
			if (armorInventory[3].getItemDamage() == color && armorInventory[2].getItemDamage() == color && armorInventory[1].getItemDamage() == color && armorInventory[0].getItemDamage() == color)
			{
				return true;
			}
		}
		return false;
	}
}
