package com.slatebit.paintball.common;

import io.netty.buffer.ByteBuf;
import java.util.Iterator;
import java.util.List;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.entity.projectile.EntityArrow;
import net.minecraft.init.Blocks;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.BlockPos;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EntityDamageSource;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.IEntityAdditionalSpawnData;
import net.minecraftforge.fml.common.registry.IThrowableEntity;

public class EntityGrenade extends EntityArrow implements IEntityAdditionalSpawnData, IThrowableEntity
{
	public int color = 0;
	private int xTile = -1;
	private int yTile = -1;
	private int zTile = -1;
	private Block inTile = null;
	private int inData = 0;
	private boolean inGround = false;
	private int ticksInGround;
	private int ticksInAir = 0;
	private double gravity;
	private boolean hasDataWatcher;

	public EntityGrenade(World world)
	{
		super(world);
		this.renderDistanceWeight = 25.0D;
		this.setSize(0.5F, 0.5F);
	}

	public EntityGrenade(World world, double x, double y, double z, double gravity, int color)
	{
		super(world);
		this.renderDistanceWeight = 25.0D;
		this.setSize(0.5F, 0.5F);
		this.setPosition(x, y, z);
		this.color = color;
		this.gravity = gravity;
		this.dataWatcher.addObject(15, color);
		this.hasDataWatcher = true;
	}

	public EntityGrenade(World world, EntityPlayer entityPlayer, double gravity, int color)
	{
		super(world);
		this.renderDistanceWeight = 25.0D;
		this.shootingEntity = entityPlayer;
		this.canBePickedUp = 0;
		this.setLocationAndAngles(entityPlayer.posX, entityPlayer.posY + entityPlayer.getEyeHeight(), entityPlayer.posZ, entityPlayer.rotationYaw, entityPlayer.rotationPitch);
		this.setPosition(this.posX, this.posY, this.posZ);
		this.motionX = 5.0 * -MathHelper.sin(this.rotationYaw / 180F * 3.141593F) * MathHelper.cos(this.rotationPitch / 180F * 3.141593F);
		this.motionZ = 5.0 * MathHelper.cos(this.rotationYaw / 180F * 3.141593F) * MathHelper.cos(this.rotationPitch / 180F * 3.141593F);
		this.motionY = 5.0 * -MathHelper.sin(this.rotationPitch / 180F * 3.141593F);
		this.setThrowableHeading(this.motionX, this.motionY, this.motionZ, 1.5F, 1.0F);
		this.color = color;
		this.gravity = gravity;
		this.dataWatcher.addObject(15, color);
		this.hasDataWatcher = true;
	}

	@Override
	public void onUpdate()
	{
		if (this.hasDataWatcher)
		{
			if (this.worldObj.isRemote)
			{
				this.color = this.dataWatcher.getWatchableObjectInt(15);
			}
			else
			{
				this.dataWatcher.updateObject(15, this.color);
			}
		}
		if (this.ticksInGround == 30)
		{
			List list = this.worldObj.getEntitiesWithinAABBExcludingEntity(this, this.getEntityBoundingBox().expand(3.5D, 3.5D, 3.5D));
			for (int i = 0; i < list.size(); ++ i)
			{
				DamageSource damageSource = new EntityDamageSource("Paintball", this.shootingEntity);
				Entity entity = (Entity) list.get(i);
				if (entity instanceof EntityPlayer)
				{
					EntityPlayer entityPlayer = (EntityPlayer) entity;
					InventoryPlayer inventory = entityPlayer.inventory;
					if (entityPlayer.inventory != null && inventory.armorInventory != null)
					{
						if (!Paintball.isSameTeam(this.color, entityPlayer.inventory.armorInventory) && !entityPlayer.capabilities.isCreativeMode)
						{
							Paintball.NETWORK_HELPER.sendPacketToPlayer(new PacketHandler(entityPlayer.getDisplayNameString(), 2), (EntityPlayer) this.shootingEntity);
							entityPlayer.hurtResistantTime = 0;
							entityPlayer.attackEntityFrom(damageSource, 8);
						}
					}
				}
				else if (entity != this.shootingEntity && entity instanceof EntityLiving)
				{
					Paintball.NETWORK_HELPER.sendPacketToPlayer(new PacketHandler(((EntityPlayer) this.shootingEntity).getDisplayNameString(), 2), (EntityPlayer) this.shootingEntity);
					EntityLiving entityLiving = (EntityLiving) entity;
					entityLiving.hurtResistantTime = 0;
					entityLiving.attackEntityFrom(damageSource, 8);
				}
			}
			this.worldObj.createExplosion(this, this.posX, this.posY, this.posZ, 0.0F, true);
			this.setDead();
		}
		if (this.prevRotationPitch == 0.0F && this.prevRotationYaw == 0.0F)
		{
			float var1 = MathHelper.sqrt_double(this.motionX * this.motionX + this.motionZ * this.motionZ);
			this.prevRotationYaw = this.rotationYaw = (float) (Math.atan2(this.motionX, this.motionZ) * 180.0D / Math.PI);
			this.prevRotationPitch = this.rotationPitch = (float) (Math.atan2(this.motionY, var1) * 180.0D / Math.PI);
		}
		IBlockState state = this.worldObj.getBlockState(new BlockPos(this.xTile, this.yTile, this.zTile));
		Block var16 = state.getBlock();
		if (var16 != Blocks.air)
		{
			var16.setBlockBoundsBasedOnState(this.worldObj, new BlockPos(this.xTile, this.yTile, this.zTile));
			AxisAlignedBB var2 = var16.getCollisionBoundingBox(this.worldObj, new BlockPos(this.xTile, this.yTile, this.zTile), state);
			if (var2 != null && var2.isVecInside(new Vec3(this.posX, this.posY, this.posZ)))
			{
				this.inGround = true;
			}
		}
		if (this.arrowShake > 0)
		{
			-- this.arrowShake;
		}
		if (this.inGround)
		{
			int var19 = var16.getMetaFromState(state);
			if (var16 == this.inTile && var19 == this.inData)
			{
				++ this.ticksInGround;
			}
			else
			{
				this.inGround = false;
				this.motionX *= this.rand.nextFloat() * 0.2F;
				this.motionY *= this.rand.nextFloat() * 0.2F;
				this.motionZ *= this.rand.nextFloat() * 0.2F;
				this.ticksInGround = 0;
				this.ticksInAir = 0;
			}
		}
		else
		{
			++ this.ticksInAir;
			Vec3 var17 = new Vec3(this.posX, this.posY, this.posZ);
			Vec3 var3 = new Vec3(this.posX + this.motionX, this.posY + this.motionY, this.posZ + this.motionZ);
			MovingObjectPosition var4 = this.worldObj.rayTraceBlocks(var17, var3, false, true, false);
			var17 = new Vec3(this.posX, this.posY, this.posZ);
			var3 = new Vec3(this.posX + this.motionX, this.posY + this.motionY, this.posZ + this.motionZ);
			if (var4 != null)
			{
				var3 = new Vec3(var4.hitVec.xCoord, var4.hitVec.yCoord, var4.hitVec.zCoord);
			}
			Entity var5 = null;
			List var6 = this.worldObj.getEntitiesWithinAABBExcludingEntity(this, this.getEntityBoundingBox().addCoord(this.motionX, this.motionY, this.motionZ).expand(1.0D, 1.0D, 1.0D));
			double var7 = 0.0D;
			Iterator var9 = var6.iterator();
			float var11;
			while (var9.hasNext())
			{
				Entity var10 = (Entity) var9.next();
				if (var10.canBeCollidedWith() && (var10 != this.shootingEntity || this.ticksInAir >= 5))
				{
					var11 = 0.3F;
					AxisAlignedBB var12 = var10.getEntityBoundingBox().expand(var11, var11, var11);
					MovingObjectPosition var13 = var12.calculateIntercept(var17, var3);
					if (var13 != null)
					{
						double var14 = var17.distanceTo(var13.hitVec);
						if (var14 < var7 || var7 == 0.0D)
						{
							var5 = var10;
							var7 = var14;
						}
					}
				}
			}
			if (var5 != null)
			{
				var4 = new MovingObjectPosition(var5);
			}
			float var20;
			if (var4 != null)
			{
				if (var4.entityHit != null)
				{
				}
				else
				{
					BlockPos blockpos1 = var4.getBlockPos();
					this.xTile = blockpos1.getX();
					this.yTile = blockpos1.getY();
					this.zTile = blockpos1.getZ();
					state = this.worldObj.getBlockState(blockpos1);
					this.inTile = state.getBlock();
					this.inData = this.inTile.getMetaFromState(state);
					this.motionX = (float) (var4.hitVec.xCoord - this.posX);
					this.motionY = (float) (var4.hitVec.yCoord - this.posY);
					this.motionZ = (float) (var4.hitVec.zCoord - this.posZ);
					var20 = MathHelper.sqrt_double(this.motionX * this.motionX + this.motionY * this.motionY + this.motionZ * this.motionZ);
					this.posX -= this.motionX / var20 * 0.05000000074505806D;
					this.posY -= this.motionY / var20 * 0.05000000074505806D;
					this.posZ -= this.motionZ / var20 * 0.05000000074505806D;
					this.worldObj.playSoundAtEntity(this, Paintball.GRENADE_LAND, 1.0F, 1.2F / (this.rand.nextFloat() * 0.2F + 0.9F));
					this.inGround = true;
					this.arrowShake = 7;
					this.setIsCritical(false);
				}
			}
			if (this.getIsCritical())
			{
				for (int var21 = 0; var21 < 4; ++ var21)
				{
					this.worldObj.spawnParticle(EnumParticleTypes.CRIT, this.posX + this.motionX * var21 / 4.0D, this.posY + this.motionY * var21 / 4.0D, this.posZ + this.motionZ * var21 / 4.0D, -this.motionX, -this.motionY + 0.2D, -this.motionZ);
				}
			}
			this.posX += this.motionX;
			this.posY += this.motionY;
			this.posZ += this.motionZ;
			var20 = MathHelper.sqrt_double(this.motionX * this.motionX + this.motionZ * this.motionZ);
			this.rotationYaw = (float) (Math.atan2(this.motionX, this.motionZ) * 180.0D / Math.PI);
			while (this.rotationPitch - this.prevRotationPitch >= 180.0F)
			{
				this.prevRotationPitch += 360.0F;
			}
			while (this.rotationYaw - this.prevRotationYaw < -180.0F)
			{
				this.prevRotationYaw -= 360.0F;
			}
			while (this.rotationYaw - this.prevRotationYaw >= 180.0F)
			{
				this.prevRotationYaw += 360.0F;
			}
			this.rotationPitch = this.prevRotationPitch + (this.rotationPitch - this.prevRotationPitch) * 0.2F;
			this.rotationYaw = this.prevRotationYaw + (this.rotationYaw - this.prevRotationYaw) * 0.2F;
			float var23 = 0.99F;
			var11 = 0.05F;
			if (this.isInWater())
			{
				for (int var26 = 0; var26 < 4; ++ var26)
				{
					float var27 = 0.25F;
					this.worldObj.spawnParticle(EnumParticleTypes.WATER_BUBBLE, this.posX - this.motionX * var27, this.posY - this.motionY * var27, this.posZ - this.motionZ * var27, this.motionX, this.motionY, this.motionZ);
				}
				var23 = 0.8F;
			}
			this.motionX *= var23;
			this.motionY *= var23;
			this.motionZ *= var23;
			this.motionY -= this.gravity;
			this.setPosition(this.posX, this.posY, this.posZ);
			this.doBlockCollisions();
		}
	}

	@Override
	public void writeEntityToNBT(NBTTagCompound nbtTagCompound)
	{
		super.writeEntityToNBT(nbtTagCompound);
		nbtTagCompound.setInteger("Color", this.color);
		nbtTagCompound.setDouble("Gravity", this.gravity);
	}

	@Override
	public void readEntityFromNBT(NBTTagCompound nbtTagCompound)
	{
		super.readEntityFromNBT(nbtTagCompound);
		this.color = nbtTagCompound.getInteger("Color");
		this.gravity = nbtTagCompound.getDouble("Gravity");
	}

	@Override
	public void writeSpawnData(ByteBuf data)
	{
		data.writeInt(this.color);
	}

	@Override
	public void readSpawnData(ByteBuf data)
	{
		this.color = data.readInt();
	}

	@Override
	public Entity getThrower()
	{
		return this.shootingEntity;
	}

	@Override
	public void setThrower(Entity entity)
	{
		this.shootingEntity = entity;
	}
}
