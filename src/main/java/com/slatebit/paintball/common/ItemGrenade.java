package com.slatebit.paintball.common;

import java.util.List;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ItemGrenade extends Item
{
	public ItemGrenade()
	{
		super();
		this.setHasSubtypes(true);
		this.maxStackSize = 2;
		this.setCreativeTab(Paintball.TAB);
		GameRegistry.registerItem(this, "grenade");
	}

	@Override
	public ItemStack onItemRightClick(ItemStack itemStack, World world, EntityPlayer entityPlayer)
	{
		if (world.isRemote && Paintball.shootTime <= 0)
		{
			Paintball.NETWORK_HELPER.sendPacketToServer(new PacketHandler(entityPlayer.getDisplayNameString(), 1));
			Paintball.shootTime = 20;
		}
		return itemStack;
	}

	@Override
	public void getSubItems(Item item, CreativeTabs creativeTabs, List list)
	{
		for (int i = 0; i < 6; i ++)
		{
			list.add(new ItemStack(item, 1, i));
		}
	}

	@Override
	@SideOnly(Side.CLIENT)
	public String getItemStackDisplayName(ItemStack itemStack)
	{
		return "Grenade (" + Paintball.COLOR_MAP.get(itemStack.getItemDamage()).charAt(0) + ")";
	}
}
