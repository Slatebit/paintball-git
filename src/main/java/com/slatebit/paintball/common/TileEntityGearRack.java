package com.slatebit.paintball.common;

public class TileEntityGearRack extends TileEntityObject
{
	@Override
	public void update()
	{
		if (this.cooldown > 0)
		{
			-- this.cooldown;
		}
		if (this.cooldown != 0)
		{
			this.getBlockType().setBlockUnbreakable();
		}
		else
		{
			this.getBlockType().setHardness(2.5F);
		}
	}

	public String getTexture(int cooldown)
	{
		if (this.cooldown >= 960)
		{
			return "6";
		}
		else if (this.cooldown >= 720 && this.cooldown < 960)
		{
			return "5";
		}
		else if (this.cooldown >= 480 && this.cooldown < 720)
		{
			return "4";
		}
		else if (this.cooldown >= 240 && this.cooldown < 480)
		{
			return "3";
		}
		else if (this.cooldown > 0 && this.cooldown < 240)
		{
			return "2";
		}
		else
		{
			return "1";
		}
	}
}