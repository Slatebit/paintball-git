package com.slatebit.paintball.common;

import java.util.List;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ItemGear extends ItemArmor
{
	public ItemGear(int type)
	{
		super(Paintball.GEAR_MATERIAL, Paintball.armorRender, type);
		this.setHasSubtypes(true);
		this.setMaxDamage(0);
		this.setCreativeTab(Paintball.TAB);
		GameRegistry.registerItem(this, Paintball.ARMOR_TYPE_MAP.get(type).toLowerCase());
	}

	@Override
	public String getArmorTexture(ItemStack itemStack, Entity entity, int slot, String type)
	{
		String color = Paintball.COLOR_MAP.get(itemStack.getItemDamage());
		if (this.armorType == 2)
		{
			return "paintball:textures/models/armor/" + color.toLowerCase() + "/Armor2.png";
		}
		else
		{
			return "paintball:textures/models/armor/" + color.toLowerCase() + "/Armor1.png";
		}
	}

	@Override
	public void getSubItems(Item item, CreativeTabs creativeTabs, List list)
	{
		for (int i = 0; i < 6; i ++)
		{
			list.add(new ItemStack(item, 1, i));
		}
	}

	@Override
	@SideOnly(Side.CLIENT)
	public String getItemStackDisplayName(ItemStack itemStack)
	{
		return Paintball.ARMOR_TYPE_MAP.get(this.armorType) + " (" + Paintball.COLOR_MAP.get(itemStack.getItemDamage()).charAt(0) + ")";
	}
}
