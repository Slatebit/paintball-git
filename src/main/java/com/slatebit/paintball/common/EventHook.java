package com.slatebit.paintball.common;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.event.FOVUpdateEvent;
import net.minecraftforge.common.util.FakePlayer;
import net.minecraftforge.event.world.BlockEvent.BreakEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class EventHook
{
	@SubscribeEvent
	public void onBreak(BreakEvent event)
	{
		EntityPlayer entityPlayer = event.getPlayer();
		if (entityPlayer != null && !(entityPlayer instanceof FakePlayer))
		{
			ItemStack itemStack = entityPlayer.getCurrentEquippedItem();
			if (itemStack != null && itemStack.getItem() instanceof ItemGun)
			{
				event.setCanceled(true);
			}
		}
	}

	@SubscribeEvent
	@SideOnly(Side.CLIENT)
	public void onFOV(FOVUpdateEvent event)
	{
		event.newfov = 1 / Paintball.zoomLevel;
	}

	@SubscribeEvent
	public void onTick(TickEvent event)
	{
		if (event.type == TickEvent.Type.CLIENT && event.phase == TickEvent.Phase.START)
		{
			if (Paintball.shootTime > 0)
			{
				Paintball.shootTime --;
			}
		}
		else if (event.type == TickEvent.Type.RENDER && event.phase == TickEvent.Phase.END)
		{
			Paintball.PROXY.renderHitMarker();
			Paintball.PROXY.renderSight();
		}
	}
}
