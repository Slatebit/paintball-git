package com.slatebit.paintball.common;

import net.minecraft.block.BlockDispenser;
import net.minecraft.dispenser.BehaviorProjectileDispense;
import net.minecraft.dispenser.IBlockSource;
import net.minecraft.dispenser.IPosition;
import net.minecraft.entity.Entity;
import net.minecraft.entity.IProjectile;
import net.minecraft.entity.projectile.EntityArrow;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.World;

public class DispenseProjectile extends BehaviorProjectileDispense
{
	private int type;

	public DispenseProjectile(int type)
	{
		this.type = type;
	}

	@Override
	public ItemStack dispenseStack(IBlockSource iBlockSource, ItemStack itemStack)
	{
		World world = iBlockSource.getWorld();
		IPosition iPosition = BlockDispenser.getDispensePosition(iBlockSource);
		EnumFacing enumFacing = BlockDispenser.getFacing(iBlockSource.getBlockMetadata());
		IProjectile iProjectile = this.getProjectileEntity(world, iPosition, itemStack);
		iProjectile.setThrowableHeading(enumFacing.getFrontOffsetX(), enumFacing.getFrontOffsetY() + 0.1F, enumFacing.getFrontOffsetZ(), this.func_82500_b(), this.func_82498_a());
		world.spawnEntityInWorld((Entity) iProjectile);
		itemStack.splitStack(1);
		return itemStack;
	}

	public IProjectile getProjectileEntity(World world, IPosition iPosition, ItemStack itemStack)
	{
		double x = iPosition.getX();
		double y = iPosition.getY();
		double z = iPosition.getZ();
		EntityArrow entityProjectile = null;
		if (this.type == 0)
		{
			entityProjectile = new EntityPellet(world, x, y, z, 4, 30, itemStack.getItemDamage());
		}
		else if (this.type == 1)
		{
			entityProjectile = new EntityGrenade(world, x, y, z, 0.035, itemStack.getItemDamage());
		}
		return entityProjectile;
	}

	@Override
	public void playDispenseSound(IBlockSource iBlockSource)
	{
		World world = iBlockSource.getWorld();
		if (this.type == 0)
		{
			world.playSoundEffect(iBlockSource.getX(), iBlockSource.getY(), iBlockSource.getZ(), Paintball.GUN_SHOOT, 1.0F, 1.0F / (world.rand.nextFloat() * 0.4F + 0.8F));
		}
		else if (this.type == 1)
		{
			world.playSoundEffect(iBlockSource.getX(), iBlockSource.getY(), iBlockSource.getZ(), Paintball.GRENADE_PIN, 1.0F, 1.0F / (world.rand.nextFloat() * 0.4F + 0.8F));
		}
	}

	@Override
	public IProjectile getProjectileEntity(World world, IPosition iPosition)
	{
		return null;
	}
}
