package com.slatebit.paintball.common;

public class TileEntityPod extends TileEntityObject
{
	@Override
	public void update()
	{
		if (this.cooldown > 0)
		{
			-- this.cooldown;
		}
		if (this.cooldown != 0)
		{
			this.getBlockType().setBlockUnbreakable();
		}
		else
		{
			this.getBlockType().setHardness(2.5F);
		}
	}

	public String getTexture(int cooldown)
	{
		if (cooldown >= 900)
		{
			return "5";
		}
		else if (cooldown >= 600 && cooldown < 900)
		{
			return "4";
		}
		else if (cooldown >= 300 && cooldown < 600)
		{
			return "3";
		}
		else if (cooldown > 0 && cooldown < 300)
		{
			return "2";
		}
		return "1";
	}
}
