package com.slatebit.paintball.common;

public class TileEntityMedKit extends TileEntityObject
{
	@Override
	public void update()
	{
		if (this.cooldown > 0)
		{
			-- this.cooldown;
		}
		if (this.cooldown != 0)
		{
			this.getBlockType().setBlockUnbreakable();
		}
		else
		{
			this.getBlockType().setHardness(2.5F);
		}
	}

	public String getTexture(int cooldown)
	{
		if (this.cooldown > 600 && this.cooldown <= 1200)
		{
			return "3";
		}
		else if (this.cooldown > 0 && this.cooldown <= 600)
		{
			return "2";
		}
		return "1";
	}
}
