package com.slatebit.paintball.common;

import java.util.ArrayList;
import java.util.List;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockPos;
import net.minecraft.util.ChatComponentText;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ItemRemote extends Item
{
	public ItemRemote()
	{
		super();
		this.setHasSubtypes(true);
		this.maxStackSize = 1;
		this.setCreativeTab(Paintball.TAB);
		GameRegistry.registerItem(this, "remote");
	}

	@Override
	public boolean onBlockStartBreak(ItemStack itemStack, BlockPos pos, EntityPlayer entityPlayer)
	{
		World world = entityPlayer.worldObj;
		IBlockState state = world.getBlockState(pos);
		Block block = state.getBlock();
		if (block == Paintball.c4)
		{
			TileEntityC4 tileEntityC4 = (TileEntityC4) world.getTileEntity(pos);
			InventoryPlayer inventory = entityPlayer.inventory;
			if (inventory != null && inventory.armorInventory != null && entityPlayer.capabilities.isCreativeMode)
			{
				int color = block.getMetaFromState(state);
				boolean isSameTeam = Paintball.isSameTeam(itemStack.getItemDamage(), entityPlayer.inventory.armorInventory);
				if (!world.isRemote)
				{
					if (isSameTeam && color == itemStack.getItemDamage() && (tileEntityC4.owner == null || tileEntityC4.owner == entityPlayer))
					{
						String key = entityPlayer.getDisplayName() + " " + itemStack.getItemDamage();
						ArrayList<TileEntity> tileEntitiesC4 = Paintball.C4_REMOTE.get(key);
						if (tileEntitiesC4 == null)
						{
							tileEntitiesC4 = new ArrayList<TileEntity>();
							Paintball.C4_REMOTE.put(key, tileEntitiesC4);
						}
						if (!tileEntitiesC4.contains(tileEntityC4))
						{
							tileEntitiesC4.add(tileEntityC4);
							tileEntityC4.owner = entityPlayer;
							entityPlayer.addChatMessage(new ChatComponentText("The C4 is now connected to the remote."));
						}
						else if (tileEntitiesC4.contains(tileEntityC4))
						{
							tileEntitiesC4.remove(tileEntityC4);
							tileEntityC4.owner = null;
							tileEntitiesC4 = null;
							entityPlayer.addChatMessage(new ChatComponentText("The C4 is no longer connected to the remote."));
						}
					}
					else if (isSameTeam && tileEntityC4.owner != null && tileEntityC4.owner != entityPlayer)
					{
						entityPlayer.addChatMessage(new ChatComponentText("The C4 is already connected to another remote."));
					}
					else
					{
						entityPlayer.addChatMessage(new ChatComponentText("You aren't on the " + Paintball.COLOR_MAP.get(color).toLowerCase() + " team."));
					}
				}
			}
			return true;
		}
		return false;
	}

	@Override
	public ItemStack onItemRightClick(ItemStack itemStack, World world, EntityPlayer entityPlayer)
	{
		if (!world.isRemote)
		{
			ArrayList<TileEntity> tileEntitiesC4 = Paintball.C4_REMOTE.get(entityPlayer.getDisplayName() + " " + itemStack.getItemDamage());
			if (tileEntitiesC4 != null && !tileEntitiesC4.isEmpty())
			{
				for (TileEntity tileEntity : tileEntitiesC4)
				{
					TileEntityC4 tileEntityC4 = (TileEntityC4) tileEntity;
					tileEntityC4.triggered = true;
				}
				tileEntitiesC4.clear();
				entityPlayer.addChatMessage(new ChatComponentText("The connected C4 has been detonated."));
			}
			else
			{
				entityPlayer.addChatMessage(new ChatComponentText("The remote does not have any C4 connected to it."));
			}
		}
		return itemStack;
	}

	@Override
	public void getSubItems(Item item, CreativeTabs creativeTabs, List list)
	{
		for (int i = 0; i < 6; i ++)
		{
			list.add(new ItemStack(item, 1, i));
		}
	}

	@Override
	@SideOnly(Side.CLIENT)
	public String getItemStackDisplayName(ItemStack itemStack)
	{
		return "Remote (" + Paintball.COLOR_MAP.get(itemStack.getItemDamage()).charAt(0) + ")";
	}
}
