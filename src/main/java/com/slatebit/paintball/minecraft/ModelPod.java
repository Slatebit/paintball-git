package com.slatebit.paintball.minecraft;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelPod extends ModelBase
{
	private ModelRenderer Base;
	private ModelRenderer Pod;

	public ModelPod()
	{
		this.textureWidth = 64;
		this.textureHeight = 32;
		this.Base = new ModelRenderer(this, 0, 0);
		this.Base.addBox(-2.5F, 0.0F, -2.5F, 5, 1, 5);
		this.Base.setRotationPoint(0.0F, 23.0F, 0.0F);
		this.Base.setTextureSize(64, 32);
		this.Base.mirror = true;
		this.setRotation(this.Base, 0.0F, 0.0F, 0.0F);
		this.Pod = new ModelRenderer(this, 0, 6);
		this.Pod.addBox(-1.5F, -0.01F, -1.5F, 3, 8, 3);
		this.Pod.setRotationPoint(0.0F, 15.0F, 0.0F);
		this.Pod.setTextureSize(64, 32);
		this.Pod.mirror = true;
		this.setRotation(this.Pod, 0.0F, 0.0F, 0.0F);
	}

	@Override
	public void render(Entity entity, float var2, float var3, float var4, float var5, float var6, float var7)
	{
		super.render(entity, var2, var3, var4, var5, var6, var7);
		this.setRotationAngles(var2, var3, var4, var5, var6, var7, entity);
		this.Base.render(var7);
		this.Pod.render(var7);
	}

	private void setRotation(ModelRenderer modelRenderer, float x, float y, float z)
	{
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}

	@Override
	public void setRotationAngles(float var1, float var2, float var3, float var4, float var5, float var6, Entity entity)
	{
		super.setRotationAngles(var1, var2, var3, var4, var5, var6, entity);
	}
}
