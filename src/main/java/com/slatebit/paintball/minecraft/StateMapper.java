package com.slatebit.paintball.minecraft;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import net.minecraft.block.Block;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.block.statemap.StateMapperBase;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

@SideOnly(Side.CLIENT)
public class StateMapper extends StateMapperBase
{
	private final IProperty property;
	private final String suffix;
	private final List listProperties;

	private StateMapper(IProperty p_i46210_1_, String p_i46210_2_, List p_i46210_3_)
	{
		this.property = p_i46210_1_;
		this.suffix = p_i46210_2_;
		this.listProperties = p_i46210_3_;
	}

	@Override
	protected ModelResourceLocation getModelResourceLocation(IBlockState p_178132_1_)
	{
		LinkedHashMap linkedhashmap = Maps.newLinkedHashMap(p_178132_1_.getProperties());
		String s;
		if (this.property == null)
		{
			s = ((ResourceLocation) Block.blockRegistry.getNameForObject(p_178132_1_.getBlock())).toString();
		}
		else
		{
			s = this.property.getName((Comparable) linkedhashmap.remove(this.property));
		}
		if (this.suffix != null)
		{
			s = s + this.suffix;
		}
		Iterator iterator = this.listProperties.iterator();
		while (iterator.hasNext())
		{
			IProperty iproperty = (IProperty) iterator.next();
			linkedhashmap.remove(iproperty);
		}
		return new ModelResourceLocation("paintball:" + s, this.getPropertyString(linkedhashmap));
	}

	StateMapper(IProperty p_i46211_1_, String p_i46211_2_, List p_i46211_3_, Object p_i46211_4_)
	{
		this(p_i46211_1_, p_i46211_2_, p_i46211_3_);
	}

	@SideOnly(Side.CLIENT)
	public static class Builder
	{
		private IProperty builderProperty;
		private String builderSuffix;
		private final List builderListProperties = Lists.newArrayList();

		public StateMapper.Builder setProperty(IProperty p_178440_1_)
		{
			this.builderProperty = p_178440_1_;
			return this;
		}

		public StateMapper.Builder setBuilderSuffix(String p_178439_1_)
		{
			this.builderSuffix = p_178439_1_;
			return this;
		}

		public StateMapper.Builder addPropertiesToIgnore(IProperty... p_178442_1_)
		{
			Collections.addAll(this.builderListProperties, p_178442_1_);
			return this;
		}

		public StateMapper build()
		{
			return new StateMapper(this.builderProperty, this.builderSuffix, this.builderListProperties, null);
		}
	}
}