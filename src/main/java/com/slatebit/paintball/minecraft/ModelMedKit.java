package com.slatebit.paintball.minecraft;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelMedKit extends ModelBase
{
	private ModelRenderer Main;
	private ModelRenderer Handle1;
	private ModelRenderer Handle2;
	private ModelRenderer Handle3;

	public ModelMedKit()
	{
		this.textureWidth = 64;
		this.textureHeight = 32;
		this.Main = new ModelRenderer(this, 0, 0);
		this.Main.addBox(0.0F, 0.0F, 0.0F, 4, 8, 12);
		this.Main.setRotationPoint(-6.0F, 16.0F, 2.0F);
		this.Main.setTextureSize(64, 32);
		this.Main.mirror = true;
		this.setRotation(this.Main, 0.0F, (float) Math.PI / 2F, 0.0F);
		this.Handle1 = new ModelRenderer(this, 15, 22);
		this.Handle1.addBox(0.0F, 0.0F, 0.0F, 1, 3, 1);
		this.Handle1.setRotationPoint(2.0F, 13.0F, 0.5F);
		this.Handle1.setTextureSize(64, 32);
		this.Handle1.mirror = true;
		this.setRotation(this.Handle1, 0.0F, (float) Math.PI / 2F, 0.0F);
		this.Handle2 = new ModelRenderer(this, 9, 22);
		this.Handle2.addBox(0.0F, 0.0F, 0.0F, 1, 3, 1);
		this.Handle2.setRotationPoint(-3.0F, 13.0F, 0.5F);
		this.Handle2.setTextureSize(64, 32);
		this.Handle2.mirror = true;
		this.setRotation(this.Handle2, 0.0F, (float) Math.PI / 2F, 0.0F);
		this.Handle3 = new ModelRenderer(this, 17, 24);
		this.Handle3.addBox(0.0F, 0.0F, 0.0F, 1, 1, 6);
		this.Handle3.setRotationPoint(-3.0F, 13.0F, 0.5F);
		this.Handle3.setTextureSize(64, 32);
		this.Handle3.mirror = true;
		this.setRotation(this.Handle3, 0.0F, (float) Math.PI / 2F, 0.0F);
	}

	@Override
	public void render(Entity entity, float var2, float var3, float var4, float var5, float var6, float var7)
	{
		super.render(entity, var2, var3, var4, var5, var6, var7);
		this.setRotationAngles(var2, var3, var4, var5, var6, var7, entity);
		this.Main.render(var7);
		this.Handle1.render(var7);
		this.Handle2.render(var7);
		this.Handle3.render(var7);
	}

	private void setRotation(ModelRenderer modelRenderer, float x, float y, float z)
	{
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}

	@Override
	public void setRotationAngles(float var1, float var2, float var3, float var4, float var5, float var6, Entity entity)
	{
		super.setRotationAngles(var1, var2, var3, var4, var5, var6, entity);
	}
}
