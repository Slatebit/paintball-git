package com.slatebit.paintball.minecraft;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelC4 extends ModelBase
{
	private ModelRenderer Strap1;
	private ModelRenderer Strap2;
	private ModelRenderer Lump;
	private ModelRenderer Body;

	public ModelC4()
	{
		this.textureWidth = 64;
		this.textureHeight = 32;
		this.Strap1 = new ModelRenderer(this, 24, 0);
		this.Strap1.addBox(0.0F, 0.0F, 0.0F, 1, 4, 8);
		this.Strap1.setRotationPoint(-3.0F, 20.0F, -4.0F);
		this.Strap1.setTextureSize(64, 32);
		this.Strap1.mirror = true;
		this.setRotation(this.Strap1, 0.0F, 0.0F, 0.0F);
		this.Strap2 = new ModelRenderer(this, 24, 0);
		this.Strap2.addBox(0.0F, 0.0F, 0.0F, 1, 4, 8);
		this.Strap2.setRotationPoint(2.0F, 20.0F, -4.0F);
		this.Strap2.setTextureSize(64, 32);
		this.Strap2.mirror = true;
		this.setRotation(this.Strap2, 0.0F, 0.0F, 0.0F);
		this.Lump = new ModelRenderer(this, 0, 0);
		this.Lump.addBox(0.0F, 0.0F, 0.0F, 2, 1, 2);
		this.Lump.setRotationPoint(-1.0F, 20.0F, -1.0F);
		this.Lump.setTextureSize(64, 32);
		this.Lump.mirror = true;
		this.setRotation(this.Lump, 0.0F, 0.0F, 0.0F);
		this.Body = new ModelRenderer(this, 0, 19);
		this.Body.addBox(0.0F, 2.0F, 0.0F, 8, 3, 6);
		this.Body.setRotationPoint(-4.0F, 19.0F, -3.0F);
		this.Body.setTextureSize(64, 32);
		this.Body.mirror = true;
		this.setRotation(this.Body, 0.0F, 0.0F, 0.0F);
	}

	@Override
	public void render(Entity entity, float var2, float var3, float var4, float var5, float var6, float var7)
	{
		super.render(entity, var2, var3, var4, var5, var6, var7);
		this.setRotationAngles(var2, var3, var4, var5, var6, var7, entity);
		this.Strap1.render(var7);
		this.Strap2.render(var7);
		this.Lump.render(var7);
		this.Body.render(var7);
	}

	private void setRotation(ModelRenderer modelRender, float xAngle, float yAngle, float zAngle)
	{
		modelRender.rotateAngleX = xAngle;
		modelRender.rotateAngleY = yAngle;
		modelRender.rotateAngleZ = zAngle;
	}
}