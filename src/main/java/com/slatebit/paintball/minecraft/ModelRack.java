package com.slatebit.paintball.minecraft;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelRack extends ModelBase
{
	ModelRenderer Base;
	ModelRenderer Rack;
	ModelRenderer Display1;
	ModelRenderer Display2;
	ModelRenderer Display3;
	ModelRenderer Display4;
	ModelRenderer Bar;
	ModelRenderer Shelf1;
	ModelRenderer Shelf2;
	ModelRenderer Shelf3;
	ModelRenderer Shelf4;

	public ModelRack()
	{
		this.textureWidth = 128;
		this.textureHeight = 64;
		this.Base = new ModelRenderer(this, 1, 1);
		this.Base.addBox(0F, 0F, 0F, 16, 2, 10);
		this.Base.setRotationPoint(-8F, 22F, -5F);
		this.Base.setTextureSize(128, 64);
		this.Base.mirror = true;
		this.setRotation(this.Base, 0F, 0F, 0F);
		this.Rack = new ModelRenderer(this, 1, 14);
		this.Rack.addBox(0F, 0F, 0F, 16, 11, 2);
		this.Rack.setRotationPoint(-8F, 11F, -1F);
		this.Rack.setTextureSize(128, 64);
		this.Rack.mirror = true;
		this.setRotation(this.Rack, 0F, 0F, 0F);
		this.Display1 = new ModelRenderer(this, 38, 44);
		this.Display1.addBox(0F, 0F, 0F, 8, 8, 0);
		this.Display1.setRotationPoint(0F, 12F, -3.1F);
		this.Display1.setTextureSize(128, 64);
		this.Display1.mirror = true;
		this.setRotation(this.Display1, 0F, 0F, 0F);
		this.Display2 = new ModelRenderer(this, 38, 34);
		this.Display2.addBox(0F, 0F, 0F, 8, 8, 0);
		this.Display2.setRotationPoint(-8F, 12F, -3.1F);
		this.Display2.setTextureSize(128, 64);
		this.Display2.mirror = true;
		this.setRotation(this.Display2, 0F, 0F, 0F);
		this.Display3 = new ModelRenderer(this, 38, 14);
		this.Display3.addBox(0F, 0F, 0F, 8, 8, 0);
		this.Display3.setRotationPoint(0F, 12F, 3.1F);
		this.Display3.setTextureSize(128, 64);
		this.Display3.mirror = true;
		this.setRotation(this.Display3, 0F, 0F, 0F);
		this.Display4 = new ModelRenderer(this, 38, 24);
		this.Display4.addBox(0F, 0F, 0F, 8, 8, 0);
		this.Display4.setRotationPoint(-8F, 12F, 3.1F);
		this.Display4.setTextureSize(128, 64);
		this.Display4.mirror = true;
		this.setRotation(this.Display4, 0F, 0F, 0F);
		this.Bar = new ModelRenderer(this, 1, 32);
		this.Bar.addBox(0F, 0F, 0F, 16, 2, 1);
		this.Bar.setRotationPoint(-8F, 9F, -0.5F);
		this.Bar.setTextureSize(128, 64);
		this.Bar.mirror = true;
		this.setRotation(this.Bar, 0F, 0F, 0F);
		this.Shelf1 = new ModelRenderer(this, 1, 28);
		this.Shelf1.addBox(0F, 0F, 0F, 16, 1, 2);
		this.Shelf1.setRotationPoint(-8F, 12F, 1F);
		this.Shelf1.setTextureSize(128, 64);
		this.Shelf1.mirror = true;
		this.setRotation(this.Shelf1, 0F, 0F, 0F);
		this.Shelf2 = new ModelRenderer(this, 1, 28);
		this.Shelf2.addBox(0F, 0F, 0F, 16, 1, 2);
		this.Shelf2.setRotationPoint(-8F, 12F, -3F);
		this.Shelf2.setTextureSize(128, 64);
		this.Shelf2.mirror = true;
		this.setRotation(this.Shelf2, 0F, 0F, 0F);
		this.Shelf3 = new ModelRenderer(this, 1, 28);
		this.Shelf3.addBox(0F, 0F, 0F, 16, 1, 2);
		this.Shelf3.setRotationPoint(-8F, 19F, 1F);
		this.Shelf3.setTextureSize(128, 64);
		this.Shelf3.mirror = true;
		this.setRotation(this.Shelf3, 0F, 0F, 0F);
		this.Shelf4 = new ModelRenderer(this, 1, 28);
		this.Shelf4.addBox(0F, 0F, 0F, 16, 1, 2);
		this.Shelf4.setRotationPoint(-8F, 19F, -3F);
		this.Shelf4.setTextureSize(128, 64);
		this.Shelf4.mirror = true;
		this.setRotation(this.Shelf4, 0F, 0F, 0F);
	}

	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
	{
		super.render(entity, f, f1, f2, f3, f4, f5);
		this.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
		this.Base.render(f5);
		this.Rack.render(f5);
		this.Display1.render(f5);
		this.Display2.render(f5);
		this.Display3.render(f5);
		this.Display4.render(f5);
		this.Bar.render(f5);
		this.Shelf1.render(f5);
		this.Shelf2.render(f5);
		this.Shelf3.render(f5);
		this.Shelf4.render(f5);
	}

	private void setRotation(ModelRenderer modelRenderer, float x, float y, float z)
	{
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}

	@Override
	public void setRotationAngles(float var1, float var2, float var3, float var4, float var5, float var6, Entity entity)
	{
		super.setRotationAngles(var1, var2, var3, var4, var5, var6, entity);
	}
}
