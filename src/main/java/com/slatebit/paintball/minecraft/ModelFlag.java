package com.slatebit.paintball.minecraft;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelFlag extends ModelBase
{
	ModelRenderer Top;
	ModelRenderer Pole;
	ModelRenderer Flag1;
	ModelRenderer Flag2;
	ModelRenderer Flag3;
	ModelRenderer Flag4;
	ModelRenderer Base1;
	ModelRenderer Base2;

	public ModelFlag()
	{
		this.textureWidth = 64;
		this.textureHeight = 32;
		this.Top = new ModelRenderer(this, 1, 20);
		this.Top.addBox(-1.5F, -15F, -1.5F, 3, 1, 3);
		this.Top.setRotationPoint(-2F, 23F, 0F);
		this.Top.setTextureSize(64, 32);
		this.Top.mirror = true;
		this.setRotation(this.Top, 0F, 0F, 0F);
		this.Pole = new ModelRenderer(this, 43, 1);
		this.Pole.addBox(-1F, -14F, -1F, 2, 13, 2);
		this.Pole.setRotationPoint(-2F, 23F, 0F);
		this.Pole.setTextureSize(64, 32);
		this.Pole.mirror = true;
		this.setRotation(this.Pole, 0F, 0F, 0F);
		this.Flag1 = new ModelRenderer(this, 15, 21);
		this.Flag1.addBox(1F, -13F, -0.5F, 2, 8, 1);
		this.Flag1.setRotationPoint(-2F, 23F, 0F);
		this.Flag1.setTextureSize(64, 32);
		this.Flag1.mirror = true;
		this.setRotation(this.Flag1, 0F, 0F, 0F);
		this.Flag2 = new ModelRenderer(this, 22, 21);
		this.Flag2.addBox(3F, -14F, -0.5F, 3, 8, 1);
		this.Flag2.setRotationPoint(-2F, 23F, 0F);
		this.Flag2.setTextureSize(64, 32);
		this.Flag2.mirror = true;
		this.setRotation(this.Flag2, 0F, 0F, 0F);
		this.Flag3 = new ModelRenderer(this, 31, 21);
		this.Flag3.addBox(6F, -13F, -0.5F, 3, 8, 1);
		this.Flag3.setRotationPoint(-2F, 23F, 0F);
		this.Flag3.setTextureSize(64, 32);
		this.Flag3.mirror = true;
		this.setRotation(this.Flag3, 0F, 0F, 0F);
		this.Flag4 = new ModelRenderer(this, 40, 21);
		this.Flag4.addBox(9F, -14F, -0.5F, 1, 8, 1);
		this.Flag4.setRotationPoint(-2F, 23F, 0F);
		this.Flag4.setTextureSize(64, 32);
		this.Flag4.mirror = true;
		this.setRotation(this.Flag4, 0F, 0F, 0F);
		this.Base1 = new ModelRenderer(this, 1, 13);
		this.Base1.addBox(-2.5F, -1F, -2.5F, 5, 1, 5);
		this.Base1.setRotationPoint(-2F, 23F, 0F);
		this.Base1.setTextureSize(64, 32);
		this.Base1.mirror = true;
		this.setRotation(this.Base1, 0F, 0F, 0F);
		this.Base2 = new ModelRenderer(this, 1, 1);
		this.Base2.addBox(-5F, 0F, -5F, 10, 1, 10);
		this.Base2.setRotationPoint(-2F, 23F, 0F);
		this.Base2.setTextureSize(64, 32);
		this.Base2.mirror = true;
		this.setRotation(this.Base2, 0F, 0F, 0F);
	}

	@Override
	public void render(Entity entity, float var2, float var3, float var4, float var5, float var6, float var7)
	{
		super.render(entity, var2, var3, var4, var5, var6, var7);
		this.setRotationAngles(var2, var3, var4, var5, var6, var7, entity);
		this.Top.render(var7);
		this.Pole.render(var7);
		this.Flag1.render(var7);
		this.Flag2.render(var7);
		this.Flag3.render(var7);
		this.Flag4.render(var7);
		this.Base1.render(var7);
		this.Base2.render(var7);
	}

	private void setRotation(ModelRenderer modelRenderer, float x, float y, float z)
	{
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}

	@Override
	public void setRotationAngles(float var1, float var2, float var3, float var4, float var5, float var6, Entity entity)
	{
		super.setRotationAngles(var1, var2, var3, var4, var5, var6, entity);
	}
}
