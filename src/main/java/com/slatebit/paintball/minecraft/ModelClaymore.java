package com.slatebit.paintball.minecraft;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelClaymore extends ModelBase
{
	private ModelRenderer Body = new ModelRenderer(this, 0, 0);
	private ModelRenderer Leg1;
	private ModelRenderer Leg2;
	private ModelRenderer Handle1;
	private ModelRenderer Handle2;
	private ModelRenderer Handle3;

	public ModelClaymore()
	{
		this.Body.addBox(-4.0F, 0.0F, -1.0F, 8, 5, 2);
		this.Body.setRotationPoint(0.0F, 17.5F, 0.0F);
		this.Body.rotateAngleX = 0.0F;
		this.Body.rotateAngleY = 0.0F;
		this.Body.rotateAngleZ = 0.0F;
		this.Body.mirror = false;
		this.Leg1 = new ModelRenderer(this, 0, 7);
		this.Leg1.addBox(-3.5F, 4.5F, -0.5F, 1, 2, 1);
		this.Leg1.setRotationPoint(0.0F, 17.5F, 0.0F);
		this.Leg1.rotateAngleX = 0.0F;
		this.Leg1.rotateAngleY = 0.0F;
		this.Leg1.rotateAngleZ = 0.0F;
		this.Leg1.mirror = false;
		this.Leg2 = new ModelRenderer(this, 0, 7);
		this.Leg2.addBox(2.5F, 4.5F, -0.5F, 1, 2, 1);
		this.Leg2.setRotationPoint(0.0F, 17.5F, 0.0F);
		this.Leg2.rotateAngleX = 0.0F;
		this.Leg2.rotateAngleY = 0.0F;
		this.Leg2.rotateAngleZ = 0.0F;
		this.Leg2.mirror = false;
		this.Handle1 = new ModelRenderer(this, 4, 7);
		this.Handle1.addBox(-0.1F, -2.0F, -0.5F, 3, 1, 1);
		this.Handle1.setRotationPoint(0.0F, 17.5F, 0.0F);
		this.Handle1.rotateAngleX = 0.0F;
		this.Handle1.rotateAngleY = 0.0F;
		this.Handle1.rotateAngleZ = 0.59341F;
		this.Handle1.mirror = false;
		this.Handle2 = new ModelRenderer(this, 4, 7);
		this.Handle2.addBox(-2.9F, -2.0F, -0.5F, 3, 1, 1);
		this.Handle2.setRotationPoint(0.0F, 17.5F, 0.0F);
		this.Handle2.rotateAngleX = 0.0F;
		this.Handle2.rotateAngleY = 0.0F;
		this.Handle2.rotateAngleZ = -0.59341F;
		this.Handle2.mirror = false;
		this.Handle3 = new ModelRenderer(this, 12, 7);
		this.Handle3.addBox(-1.0F, -1.73F, -0.5F, 2, 1, 1);
		this.Handle3.setRotationPoint(0.0F, 17.5F, 0.0F);
		this.Handle3.rotateAngleX = 0.0F;
		this.Handle3.rotateAngleY = 0.0F;
		this.Handle3.rotateAngleZ = 0.0F;
		this.Handle3.mirror = false;
	}

	@Override
	public void render(Entity entity, float var2, float var3, float var4, float var5, float var6, float var7)
	{
		super.render(entity, var2, var3, var4, var5, var6, var7);
		this.setRotationAngles(var2, var3, var4, var5, var6, var7, entity);
		this.Body.render(var7);
		this.Leg1.render(var7);
		this.Leg2.render(var7);
		this.Handle1.render(var7);
		this.Handle2.render(var7);
		this.Handle3.render(var7);
	}

	@Override
	public void setRotationAngles(float var1, float var2, float var3, float var4, float var5, float var6, Entity entity)
	{
		super.setRotationAngles(var1, var2, var3, var4, var5, var6, entity);
	}
}
