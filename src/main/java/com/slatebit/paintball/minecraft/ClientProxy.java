package com.slatebit.paintball.minecraft;

import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.BlockModelShapes;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.WorldRenderer;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.resources.model.ModelBakery;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.client.FMLClientHandler;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import org.lwjgl.input.Mouse;
import com.slatebit.paintball.common.BlockScanner;
import com.slatebit.paintball.common.CommonProxy;
import com.slatebit.paintball.common.EntityGrenade;
import com.slatebit.paintball.common.EntityPellet;
import com.slatebit.paintball.common.ItemGun;
import com.slatebit.paintball.common.Paintball;
import com.slatebit.paintball.common.TileEntityC4;
import com.slatebit.paintball.common.TileEntityClaymore;
import com.slatebit.paintball.common.TileEntityFlag;
import com.slatebit.paintball.common.TileEntityGearRack;
import com.slatebit.paintball.common.TileEntityMedKit;
import com.slatebit.paintball.common.TileEntityPod;
import com.slatebit.paintball.common.TileEntityWeaponRack;

public class ClientProxy extends CommonProxy
{
	private ResourceLocation hitMarker = new ResourceLocation("paintball", "textures/overlays/Hit Marker.png");

	@Override
	public void addRenders()
	{
		Paintball.mouseSensitivity = Minecraft.getMinecraft().gameSettings.mouseSensitivity;
		RenderManager renderManager = FMLClientHandler.instance().getClient().getRenderManager();
		RenderingRegistry.registerEntityRenderingHandler(EntityPellet.class, new RenderProjectile(renderManager));
		RenderingRegistry.registerEntityRenderingHandler(EntityGrenade.class, new RenderProjectile(renderManager));
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityClaymore.class, new RenderObject(new ModelClaymore(), "Claymore"));
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityC4.class, new RenderObject(new ModelC4(), "C4"));
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityFlag.class, new RenderObject(new ModelFlag(), "Flag"));
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityPod.class, new RenderObject(new ModelPod(), "Pod"));
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityWeaponRack.class, new RenderObject(new ModelRack(), "Weapon Rack"));
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityGearRack.class, new RenderObject(new ModelRack(), "Gear Rack"));
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityMedKit.class, new RenderObject(new ModelMedKit(), "Med Kit"));
		BlockModelShapes blockModelShapes = Minecraft.getMinecraft().getBlockRendererDispatcher().getBlockModelShapes();
		blockModelShapes.registerBlockWithStateMapper(Paintball.scanner, new StateMapper.Builder().setProperty(BlockScanner.COLOR).setBuilderSuffix("Scanner").build());
		blockModelShapes.registerBuiltInBlocks(Paintball.claymore);
		blockModelShapes.registerBuiltInBlocks(Paintball.c4);
		blockModelShapes.registerBuiltInBlocks(Paintball.flag);
		blockModelShapes.registerBuiltInBlocks(Paintball.pod);
		blockModelShapes.registerBuiltInBlocks(Paintball.weaponRack);
		blockModelShapes.registerBuiltInBlocks(Paintball.gearRack);
		blockModelShapes.registerBuiltInBlocks(Paintball.medKit);
		String[] colors = {"R", "O", "Y", "G", "B", "P"};
		for (int i = 0; i < 6; i ++)
		{
			this.registerBlockTexture(Paintball.instaBase, i, "Insta-Base" + colors[i]);
			this.registerBlockTexture(Paintball.scanner, i, "Scanner" + colors[i]);
			this.registerItemTexture(Paintball.helmet, i, "Helmet" + colors[i]);
			this.registerItemTexture(Paintball.chest, i, "Chest" + colors[i]);
			this.registerItemTexture(Paintball.pants, i, "Pants" + colors[i]);
			this.registerItemTexture(Paintball.shoes, i, "Shoes" + colors[i]);
			this.registerItemTexture(Paintball.remote, i, "Remote" + colors[i]);
			this.registerItemTexture(Paintball.paintbrush, i, "Paintbrush" + colors[i]);
			this.registerItemTexture(Paintball.pellet, i, "Pellet" + colors[i]);
			this.registerItemTexture(Paintball.pistol, i, "Pistol" + colors[i]);
			this.registerItemTexture(Paintball.shotgun, i, "Shotgun" + colors[i]);
			this.registerItemTexture(Paintball.rifle, i, "Rifle" + colors[i]);
			this.registerItemTexture(Paintball.sniper, i, "Sniper" + colors[i]);
			this.registerItemTexture(Paintball.launcher, i, "Launcher" + colors[i]);
			this.registerItemTexture(Paintball.grenade, i, "Grenade" + colors[i]);
			this.registerBlockTexture(Paintball.claymore, i, "Claymore" + colors[i]);
			this.registerBlockTexture(Paintball.c4, i, "C4" + colors[i]);
			this.registerBlockTexture(Paintball.flag, i, "Flag" + colors[i]);
			this.registerBlockTexture(Paintball.pod, i, "Pod" + colors[i]);
			this.registerBlockTexture(Paintball.weaponRack, i, "Weapon Rack" + colors[i]);
			this.registerBlockTexture(Paintball.gearRack, i, "Gear Rack" + colors[i]);
			this.registerBlockTexture(Paintball.medKit, i, "Med Kit" + colors[i]);
		}
	}

	public void registerBlockTexture(final Block block, final String blockName)
	{
		this.registerBlockTexture(block, 0, blockName);
	}

	public void registerBlockTexture(final Block block, int meta, final String blockName)
	{
		RenderItem renderItem = Minecraft.getMinecraft().getRenderItem();
		renderItem.getItemModelMesher().register(Item.getItemFromBlock(block), meta, new ModelResourceLocation("paintball:" + blockName, "inventory"));
		ModelBakery.addVariantName(Item.getItemFromBlock(block), "paintball:" + blockName);
	}

	public void registerItemTexture(final Item item, final String itemName)
	{
		this.registerItemTexture(item, 0, itemName);
	}

	public void registerItemTexture(final Item item, int meta, final String itemName)
	{
		RenderItem renderItem = Minecraft.getMinecraft().getRenderItem();
		renderItem.getItemModelMesher().register(item, meta, new ModelResourceLocation("paintball:" + itemName, "inventory"));
		ModelBakery.addVariantName(item, "paintball:" + itemName);
	}

	@Override
	public void renderHitMarker()
	{
		if (Paintball.hitMarker > 0)
		{
			Paintball.hitMarker --;
			Minecraft client = FMLClientHandler.instance().getClient();
			ScaledResolution scale = new ScaledResolution(client, client.displayWidth, client.displayHeight);
			int xCenter = scale.getScaledWidth() / 2;
			int offset = scale.getScaledHeight() * 2;
			client.getTextureManager().bindTexture(this.hitMarker);
			Tessellator tessellator = Tessellator.getInstance();
			WorldRenderer worldrenderer = tessellator.getWorldRenderer();
			worldrenderer.startDrawingQuads();
			worldrenderer.addVertexWithUV(xCenter - offset, scale.getScaledHeight(), 1.0D, 0.0D, 1.0D);
			worldrenderer.addVertexWithUV(xCenter + offset, scale.getScaledHeight(), 1.0D, 1.0D, 1.0D);
			worldrenderer.addVertexWithUV(xCenter + offset, 0, 1.0D, 1.0D, 0.0D);
			worldrenderer.addVertexWithUV(xCenter - offset, 0, 1.0D, 0.0D, 0.0D);
			tessellator.draw();
		}
	}

	@Override
	public void renderSight()
	{
		Minecraft client = FMLClientHandler.instance().getClient();
		if (client.thePlayer != null)
		{
			InventoryPlayer inventory = client.thePlayer.inventory;
			if (Mouse.isButtonDown(0) && inventory.getCurrentItem() != null && inventory.getCurrentItem().getItem() instanceof ItemGun && client.currentScreen == null)
			{
				ItemGun gun = (ItemGun) inventory.getCurrentItem().getItem();
				float newZoom;
				if (gun.type != "Sniper")
				{
					newZoom = 2.50F;
				}
				else
				{
					newZoom = 5.00F;
				}
				if (Paintball.zoomLevel <= newZoom)
				{
					Paintball.zoomLevel += 0.25;
				}
				if (Paintball.zoomLevel > newZoom)
				{
					Paintball.zoomLevel -= 0.25;
				}
				client.gameSettings.mouseSensitivity = Paintball.mouseSensitivity / (1 + (Paintball.zoomLevel - 1) / Paintball.zoomLevel);
				ScaledResolution scale = new ScaledResolution(client, client.displayWidth, client.displayHeight);
				int xCenter = scale.getScaledWidth() / 2;
				int offset = scale.getScaledHeight() * 2;
				client.getTextureManager().bindTexture(new ResourceLocation("paintball", "textures/overlays/" + gun.type + " Sight.png"));
				Tessellator tessellator = Tessellator.getInstance();
				WorldRenderer worldrenderer = tessellator.getWorldRenderer();
				worldrenderer.startDrawingQuads();
				worldrenderer.addVertexWithUV(xCenter - offset, scale.getScaledHeight(), -100.0D, 0.0D, 1.0D);
				worldrenderer.addVertexWithUV(xCenter + offset, scale.getScaledHeight(), -100.0D, 1.0D, 1.0D);
				worldrenderer.addVertexWithUV(xCenter + offset, 0, -100.0D, 1.0D, 0.0D);
				worldrenderer.addVertexWithUV(xCenter - offset, 0, -100.0D, 0.0D, 0.0D);
				tessellator.draw();
			}
			else
			{
				if (Paintball.zoomLevel >= 1.0)
				{
					Paintball.zoomLevel -= 0.25;
				}
				if (Paintball.zoomLevel < 1.0)
				{
					Paintball.zoomLevel = 1.0F;
				}
				client.gameSettings.mouseSensitivity = (float) (Paintball.mouseSensitivity / (1 + (Paintball.zoomLevel - 1.0) / Paintball.zoomLevel));
			}
		}
	}
}
