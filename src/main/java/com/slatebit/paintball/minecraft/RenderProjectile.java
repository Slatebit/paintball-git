package com.slatebit.paintball.minecraft;

import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.WorldRenderer;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.projectile.EntityArrow;
import net.minecraft.util.MathHelper;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.lwjgl.opengl.GL11;
import com.slatebit.paintball.common.EntityGrenade;
import com.slatebit.paintball.common.EntityPellet;
import com.slatebit.paintball.common.Paintball;

@SideOnly(Side.CLIENT)
public class RenderProjectile extends Render
{
	protected RenderProjectile(RenderManager renderManager)
	{
		super(renderManager);
	}

	private static ResourceLocation field_110780_a;

	public void renderArrow(EntityArrow entityArrow, double par2, double par4, double par6, float par8, float par9)
	{
		if (entityArrow instanceof EntityPellet)
		{
			EntityPellet entityPellet = (EntityPellet) entityArrow;
			if (Paintball.COLOR_MAP.get(entityPellet.color) != null)
			{
				RenderProjectile.field_110780_a = new ResourceLocation("paintball:textures/models/entities/" + Paintball.COLOR_MAP.get(entityPellet.color).toLowerCase() + "/Pellet.png");
			}
			else
			{
				RenderProjectile.field_110780_a = new ResourceLocation("paintball:textures/models/entities/red/Pellet.png");
			}
		}
		else if (entityArrow instanceof EntityGrenade)
		{
			EntityGrenade entityGrenade = (EntityGrenade) entityArrow;
			if (Paintball.COLOR_MAP.get(entityGrenade.color) != null)
			{
				RenderProjectile.field_110780_a = new ResourceLocation("paintball:textures/models/entities/" + Paintball.COLOR_MAP.get(entityGrenade.color).toLowerCase() + "/Grenade.png");
			}
			else
			{
				RenderProjectile.field_110780_a = new ResourceLocation("paintball:textures/models/entities/red/Grenade.png");
			}
		}
		this.bindEntityTexture(entityArrow);
		GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
		GlStateManager.pushMatrix();
		GlStateManager.translate((float) par2, (float) par4, (float) par6);
		GlStateManager.rotate(entityArrow.prevRotationYaw + (entityArrow.rotationYaw - entityArrow.prevRotationYaw) * par9 - 90.0F, 0.0F, 1.0F, 0.0F);
		GlStateManager.rotate(entityArrow.prevRotationPitch + (entityArrow.rotationPitch - entityArrow.prevRotationPitch) * par9, 0.0F, 0.0F, 1.0F);
		Tessellator tessellator = Tessellator.getInstance();
		WorldRenderer worldrenderer = tessellator.getWorldRenderer();
		byte b0 = 0;
		float f2 = 0.0F;
		float f3 = 0.5F;
		float f4 = (0 + b0 * 10) / 32.0F;
		float f5 = (5 + b0 * 10) / 32.0F;
		float f6 = 0.0F;
		float f7 = 0.15625F;
		float f8 = (5 + b0 * 10) / 32.0F;
		float f9 = (10 + b0 * 10) / 32.0F;
		float f10 = 0.05625F;
		GlStateManager.enableRescaleNormal();
		float f11 = entityArrow.arrowShake - par9;
		if (f11 > 0.0F)
		{
			float f12 = -MathHelper.sin(f11 * 3.0F) * f11;
			GlStateManager.rotate(f12, 0.0F, 0.0F, 1.0F);
		}
		GlStateManager.rotate(45.0F, 1.0F, 0.0F, 0.0F);
		GlStateManager.scale(f10, f10, f10);
		GlStateManager.translate(-4.0F, 0.0F, 0.0F);
		GL11.glNormal3f(f10, 0.0F, 0.0F);
		worldrenderer.startDrawingQuads();
		worldrenderer.addVertexWithUV(-7.0D, -2.0D, -2.0D, f6, f8);
		worldrenderer.addVertexWithUV(-7.0D, -2.0D, 2.0D, f7, f8);
		worldrenderer.addVertexWithUV(-7.0D, 2.0D, 2.0D, f7, f9);
		worldrenderer.addVertexWithUV(-7.0D, 2.0D, -2.0D, f6, f9);
		tessellator.draw();
		GL11.glNormal3f(-f10, 0.0F, 0.0F);
		worldrenderer.startDrawingQuads();
		worldrenderer.addVertexWithUV(-7.0D, 2.0D, -2.0D, f6, f8);
		worldrenderer.addVertexWithUV(-7.0D, 2.0D, 2.0D, f7, f8);
		worldrenderer.addVertexWithUV(-7.0D, -2.0D, 2.0D, f7, f9);
		worldrenderer.addVertexWithUV(-7.0D, -2.0D, -2.0D, f6, f9);
		tessellator.draw();
		for (int i = 0; i < 4; ++ i)
		{
			GlStateManager.rotate(90.0F, 1.0F, 0.0F, 0.0F);
			GL11.glNormal3f(0.0F, 0.0F, f10);
			worldrenderer.startDrawingQuads();
			worldrenderer.addVertexWithUV(-8.0D, -2.0D, 0.0D, f2, f4);
			worldrenderer.addVertexWithUV(8.0D, -2.0D, 0.0D, f3, f4);
			worldrenderer.addVertexWithUV(8.0D, 2.0D, 0.0D, f3, f5);
			worldrenderer.addVertexWithUV(-8.0D, 2.0D, 0.0D, f2, f5);
			tessellator.draw();
		}
		GlStateManager.disableRescaleNormal();
		GlStateManager.popMatrix();
	}

	protected ResourceLocation func_110779_a(EntityArrow entityArrow)
	{
		return RenderProjectile.field_110780_a;
	}

	@Override
	protected ResourceLocation getEntityTexture(Entity entity)
	{
		return this.func_110779_a((EntityArrow) entity);
	}

	@Override
	public void doRender(Entity entity, double par2, double par4, double par6, float par8, float par9)
	{
		this.renderArrow((EntityArrow) entity, par2, par4, par6, par8, par9);
	}
}
