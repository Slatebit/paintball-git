package com.slatebit.paintball.minecraft;

import net.minecraft.client.Minecraft;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.client.FMLClientHandler;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.lwjgl.opengl.GL11;
import com.slatebit.paintball.common.Paintball;
import com.slatebit.paintball.common.TileEntityGearRack;
import com.slatebit.paintball.common.TileEntityMedKit;
import com.slatebit.paintball.common.TileEntityObject;
import com.slatebit.paintball.common.TileEntityPod;
import com.slatebit.paintball.common.TileEntityWeaponRack;

@SideOnly(Side.CLIENT)
public class RenderObject extends TileEntitySpecialRenderer
{
	private Minecraft client = FMLClientHandler.instance().getClient();
	private ModelBase modelBase;
	private String type;

	public RenderObject(ModelBase modelBase, String type)
	{
		this.modelBase = modelBase;
		this.type = type;
	}

	@Override
	public void renderTileEntityAt(TileEntity tileEntity, double x, double y, double z, float partialTickTime, int i)
	{
		TileEntityObject tileEntityObject = (TileEntityObject) tileEntity;
		GL11.glPushMatrix();
		GL11.glTranslatef((float) (x + 0.5), (float) (y + 1.5), (float) (z + 0.5));
		GL11.glRotatef(180, 1, 0, 0);
		if (this.type == "Claymore")
		{
			GL11.glRotatef(tileEntityObject.direction * 360 / 16 + 180, 0.0F, 1.0F, 0.0F);
		}
		else if (this.type != "Flag")
		{
			GL11.glRotatef(90 * tileEntityObject.direction, 0, 1, 0);
		}
		else
		{
			GL11.glRotatef(90 * tileEntityObject.direction + 180, 0, 1, 0);
		}
		int cooldown = tileEntityObject.cooldown;
		String texture = "";
		if (tileEntityObject instanceof TileEntityPod)
		{
			texture = ((TileEntityPod) tileEntityObject).getTexture(cooldown);
		}
		else if (tileEntityObject instanceof TileEntityMedKit)
		{
			texture = ((TileEntityMedKit) tileEntityObject).getTexture(cooldown);
		}
		else if (tileEntityObject instanceof TileEntityWeaponRack)
		{
			texture = ((TileEntityWeaponRack) tileEntityObject).getTexture(cooldown);
		}
		else if (tileEntityObject instanceof TileEntityGearRack)
		{
			texture = ((TileEntityGearRack) tileEntityObject).getTexture(cooldown);
		}
		this.client.getTextureManager().bindTexture(new ResourceLocation("paintball:textures/models/entities/" + Paintball.COLOR_MAP.get(tileEntity.getBlockMetadata()).toLowerCase() + "/" + this.type + texture + ".png"));
		this.modelBase.render(null, 0F, 0F, 0F, 0F, 0F, 0.0625F);
		GL11.glPopMatrix();
	}
}